/**
 * 
 */
package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ListView;

/**
 * This class represents the inner representation of a presentation. It contains the meta informations and data structures for all slides.
 * @author Johannes Holtz
 * 
 */
public abstract class Presentation {

	private static final String tag = "PRESENTATION";
	protected Context mParent;
	protected SurfaceHolder mHolder;
	protected SurfaceView mSlideView;
	protected String mProjectPath;
	protected String mProjectName;
	protected List<Slide> mSlides;
	protected Thread mDrawThread;
	protected Lock mPageInfoLock;
	protected int mCurPageNum;
	protected int mCurSubPageNum;
	protected int mPageToRender;
	protected int mSubPageToRender;
	protected int mPageCount;

	public Presentation(Context parent, String path, String projName) {
		mProjectPath = path;
		mProjectName = projName;
		mParent = parent;
		mSlides = new LinkedList<Slide>();
		mPageInfoLock = new ReentrantLock();
	}

	public void setSurfaceView(SurfaceView sv) {
		mSlideView = sv;
	}

	public String getProjectPath() {
		return mProjectPath;
	}

	public String getProjectName() {
		return mProjectName;
	}

	public void setSlides(List<Slide> slides) {
		mSlides = slides;
	}

	public abstract void buildSlides(Object o);

	/***
	 * This Method gets the Slide specified by the page and subpage indexes. This must not be the page number.
	 * 
	 * @param pagenumber
	 *            The position in the pages list
	 * @param subpagenumer
	 *            The position in the minor subpages list
	 * @return The {@link Slide} object at the specified list positions
	 */
	public Slide getMainSlide(int pagenumber) {
		try{
			return mSlides.get(pagenumber);
		} catch(IndexOutOfBoundsException e){
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 * Builds a string from the project name and the page and subpage number to identify an image.
	 * @param pagein	pagenumber of the slide
	 * @param subpagein	subpagenumber of the slide
	 * @return	a string which is build after the following scheme: >>[ProjectName0:2][PageNumber]_[SubPageNumber].PNG<<
	 */
	public String getImageName(int pagein, int subpagein) {
		String imageName;
		String page = Integer.toString(pagein);
		String subpage = Integer.toString(subpagein);

		if (mProjectName.length() >= 3) {
			// name agreement for slide images >>[ProjecktFolder]/Images/[ProjectName0:2][PageNumber]_[SubPageNumber].PNG<<
			imageName = mProjectName.substring(0, 2) + page + "_" + subpage + ".PNG";
		} else {
			imageName = "unknown" + page + "_" + subpage + ".PNG";
		}
		return imageName;
	}

	public boolean hasSlides() {
		if (mSlides == null | mSlides.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public int getNumberOfMainpages() {
		return mSlides.size();
	}

	/**
	 * gets the number of slides including subpages
	 * @return the number of mainpages plus the number of subpages
	 */
	public int getNumberOfSlides() {
		int i = 0;
		for (Slide p : mSlides) {
			i++;
			i += p.getSizeofSubpages();
		}
		return i;
	}

	/**
	 * compresses a given picture and saves it in the Image folder of the project as background image for the slide where it can be used as cached picture.
	 * @param bitmap	the image which will be compressed and saved
	 * @param page		the page number of the image
	 * @param subpage	the subpage number of the image
	 */
	public void saveImage(Bitmap bitmap, int page, int subpage) {
		// save bmp to sdcard
		Log.d(tag, "Compressing Image and save to external storage");
		File picDir = new File(mProjectPath + File.separator + "Images");
		if (!picDir.exists()) {
			picDir.mkdirs();
		}
		String imageName = getImageName(page, subpage);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(picDir.getAbsolutePath() + File.separator + imageName);
			if (!bitmap.compress(CompressFormat.PNG, 0, fos)) {
				Log.d(tag, "Image compression failed");
			} else {
				Log.d(tag, "Image compression succeeded");
				fos.close();
				// mSlides.get(page).getSubpage(subpage).setPagePath(picDir.getAbsolutePath() + File.separator + imageName);
			}
		} catch (FileNotFoundException e) {
			Log.d(tag, e.getMessage());
		} catch (IOException e) {
			Log.d(tag, "error occured while closing the iage file stream " + e.getMessage());
		}
	}

	/**
	 * notifies the render thread to render new picture or prints the cached image.
	 * 
	 * @param page
	 *            the page number ( not the index ) of the page
	 * @param subpage
	 *            the subpage number ( not the index )
	 */
	public void setPageToRender(int page, int subpage) {
		mPageInfoLock.lock();
		if (pageExist(page, subpage)) {
			Log.d(tag, "page " + page + "." + subpage + " exist");
			mCurPageNum = page;
			mCurSubPageNum = subpage;
			drawPage();
		}
		mPageInfoLock.unlock();
	}

	/**
	 * searches for a page with the given page numbers. the numbers musn't be the indices of the slides.
	 * @param page		the page number
	 * @param subpage	the sub page number
	 * @return	true if a page was found, false otherwise
	 */
	private boolean pageExist(int page, int subpage) {
		for (Slide s : mSlides) {
			if (page == s.getPageNumber()) {
				//right main page found
				if (subpage == 0) {
					//found 
					return true;
				}else{
					//search subpage list for slide
					for (int i = 0; i < s.getSizeofSubpages(); i++) {
						if(subpage == s.getSubpage(i).getPageNumber()){
							//found correct subpage
							return true;
						}
					}
				}
				//page cannot be found in another trunk
				return false;
			}
		}
		return false;
	}

	
	public void setPageToRender(Slide s) {
		mPageInfoLock.lock();
		if (!isCached(s)) {
			Log.d(tag, "file was not found in cache");
		}
		mPageInfoLock.unlock();
	}

	protected void drawPage() {
		if (!isCached()) {
			Log.d(tag, "file was not found in cache");
			// set the page to be drawn
			// the drawing thread will pick it up.
			mPageToRender = mCurPageNum;
			mSubPageToRender = mCurSubPageNum;
		}
	}

	private boolean isCached(Slide s) {
		String path = s.getPagePath();
		File f = new File(mProjectPath + File.separator + path);
		if (f.exists() && f.isFile()) {
			if (!drawCachedImage(f.getAbsolutePath())) {
				Log.d(tag, "error while drawing File from cache");
			}
			return true;
		} else {
			return false;
		}

	}

	private boolean drawCachedImage(String path) {
		Canvas c = null;
		try {
			c = mHolder.lockCanvas(null);
			synchronized (mHolder) {
				// TODO eventually Bitmap modification here (scaling, clipping, etc.)
				Bitmap b = BitmapFactory.decodeFile(path);
				if (b == null) {
					Log.d(tag, "Given PNG file cannot be decoded");
				}
				c.drawBitmap(b, 0, 0, null);
			}
		} catch (Exception e) {
			Log.d(tag, "exception while drawing cached file: " + e.getMessage());
			e.printStackTrace();
			return false;
		} finally {
			if (c != null) {
				mHolder.unlockCanvasAndPost(c);
			}
			Log.d(tag, "finished drawing");
		}
		return true;
	}

	/***
	 * Checks is the image specified by the current page and subpagenumber in this presentation object. If cashed it draws it at once.
	 * 
	 * @return false if image was not found, true if it was drawn
	 */
	private boolean isCached() {
		// check if drawn page is in cache; if not, draw it.
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			String path = mProjectPath + File.separator + "Images" + File.separator + getImageName(mCurPageNum, mCurSubPageNum);
			Log.d(tag, "looking for " + path);
			File f = new File(path);
			if (f.exists() && f.isFile()) {
				Log.d(tag, "file was found in cache in order to draw it: " + path);
				drawCachedImage(path);
				return true;
			} else {
				Log.d(tag, "File was not found in Cache " + path);
				return false;
			}
		} else {
			Log.d(tag, "can not access external storage");
		}
		return false;
	}

	/***
	 * Gets the next Slide according to the list structure. It recognizes if the page has subpages or not.
	 * 
	 * @param s
	 *            The {@link Slide} Object from which the next will be returned
	 * @return The {@link Slide} object of the following Slide to {@link s}. If s is the last Slide it returns NULL.
	 */
	public Slide getNext(Slide s) {
		if (isLast(s)) {
			return null;
		} else {
			if (s.isMainpage()) {
				if (s.hasSubPages()) {
					return s.getSubpage(0);
				} else {
					return mSlides.get(getIndex(s) + 1);
				}
			} else {
				if (getIndex(s) < s.getParent().getSizeofSubpages() - 1) {
					return s.getParent().getSubpage(getIndex(s) + 1);
				} else {
					return mSlides.get(getIndex(s.getParent()) + 1);
				}
			}
		}
	}

	public Slide getPrev(Slide s) {
		if (s.equals(mSlides.get(0))) {
			return null;
		} else {
			if (s.isMainpage()) {
				Slide prevmain = mSlides.get(getIndex(s) - 1);
				if (prevmain.hasSubPages()) {
					return prevmain.getSubpage(prevmain.getSizeofSubpages() - 1);
				} else {
					return prevmain;
				}
			} else {
				if (getIndex(s) > 0) {
					return s.getParent().getSubpage(getIndex(s) - 1);
				} else {
					return s.getParent();
				}
			}
		}
	}

	public boolean isLast(Slide s) {
		if (s.isMainpage()) {
			if (s.hasSubPages()) {
				return false;
			} else if (mSlides.indexOf(s) == (mSlides.size() - 1)) {
				Log.d(tag, "last Slide detected. pagenumber is " + s.getPageNumber());
				return true;
			} else {
				return false;
			}
		} else {
			if (getIndex(s) == s.getParent().getSizeofSubpages() - 1) {
				if (getIndex(s.getParent()) == mSlides.size() - 1) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	public int getIndex(Slide s) {
		if (s.isMainpage()) {
			return mSlides.indexOf(s);
		} else {
			for (int i = 0; i < s.getParent().getSizeofSubpages(); i++) {
				if (s.getParent().getSubpage(i).equals(s)) {
					return i;
				}
			}
			return -1;
		}
	}

	/***
	 * Gets the {@link Slide} object according the position in the {@link ListView}.
	 * 
	 * @param listnumber
	 *            position in the {@link ListView}
	 * @return the {@link Slide} object at the given position
	 */
	public Slide getSlide(int listnumber) {
		Slide s = mSlides.get(0);
		for (int i = 0; i < listnumber; i++) {
			s = getNext(s);
		}
		return s;
	}

	public abstract void createDrawThread();

	public abstract void startThread();

	public abstract void stopThread();

	public abstract void changedLayout(int l, int t, int r, int b);

	public void setHolder(SurfaceHolder holder) {
		mHolder = holder;
	}

}
