package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import dipl.dist.RapidAuthoringApp.R.id;
/**
 * This activity shows a list of the last 5 used files. If a new presentations was created the corresponding tmp file is shown. 
 * @author Johannes Holtz
 *
 */
public class RecentFileChooser extends Activity implements OnItemClickListener {

	private static final String tag = "FileChooserRecord";
	private ListView mListView;
	private ArrayAdapter<String> mArAdapter;
	private ArrayList<String> mRecentFileList = new ArrayList<String>(5);
	private static final int REQUEST_CODE = 10;
	private static final boolean DIRCETORYCHOOSER = true;
	private static final boolean FILECHOOSER = false;
	private static boolean isPlayer = false;
	private static boolean isRecorder = false;
	private static boolean isEditor = false;
	private static boolean emptyProject = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_chooser_record);
		mListView = (ListView) findViewById(id.lv_recent_files);
		mArAdapter = new FileListAdapter(this, R.layout.extended_listitem_filechooser, R.id.tv_praefix, null);
		mListView.setAdapter(mArAdapter);
		mListView.setOnItemClickListener(this);

		Intent i = getIntent();
		switch (i.getIntExtra("FILTER_TYPE", 0)) {
		case 1: {
			isPlayer = true;
			isEditor = false;
			isRecorder = false;
			Log.d(tag, "Player mode");
			break;
		}
		case 2: {
			isPlayer = false;
			isRecorder = true;
			isEditor = false;
			Log.d(tag, "Recorder mode");
			break;
		}
		case 3: {
			isPlayer = false;
			isRecorder = false;
			isEditor = true;
			Log.d(tag, "Editor mode");
			break;
		}
		default: {
			Log.d(tag, "Filter type is unknown");
		}
		}
		
		if(isPlayer | isEditor){
			Button btn = (Button)findViewById(R.id.btn_newProject);
			btn.setEnabled(false);
		}
		SharedPreferences prefs = getPrefs();

		SharedPreferences.Editor editor = prefs.edit();
		editor.commit();
	}

	@Override
	protected void onResume() {
		super.onResume();
				
		populate(mArAdapter, mRecentFileList);
	}

	private void populate(ArrayAdapter<String> adapter, ArrayList<String> list){
		SharedPreferences prefs = getPrefs();
		list.clear();
		purgeDuplicats(list);
		list.add(prefs.getString("listItem1", ""));
		list.add(prefs.getString("listItem2", ""));
		list.add(prefs.getString("listItem3", ""));
		list.add(prefs.getString("listItem4", ""));
		list.add(prefs.getString("listItem5", ""));
		
		adapter.clear();
		for (String s : list) {
			int index = s.lastIndexOf(File.separator) == -1 ? 0 : s.lastIndexOf(File.separator);
			if (index == 0) {
				adapter.add(s);
			} else {
				//just display the Project Name + suffix
				String filename = s.substring(index + 1);
//				int pointindex = filename.indexOf(".");
//				pointindex = pointindex==-1?filename.length():pointindex;
//				adapter.add(filename.substring(0,pointindex));
				adapter.add(filename);
			}
		}
	}
	
	private void purgeDuplicats(ArrayList<String> rfl) {
		for (int i = 0; i < rfl.size(); i++) {
			// for every entry in the list
			for (int j = 0; j < rfl.size(); j++) {
				// compare every other entry
				if ((j != i) & !rfl.get(i).equals("") & rfl.get(i).equals(rfl.get(j))) {
					// ignore itself
					// compare only if not empty
					// if equal set entry empty
					rfl.set(j, "");
				}
			}
		}

	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		String selectedFile = mRecentFileList.get(position);
		if (selectedFile.equals(""))
			return;
		startToolset(selectedFile);
		Log.d(tag, "selected File: " + selectedFile);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_file_chooser_record, menu);
		return true;
	}

	public void onClickChoose(View v) {
		Intent intent = new Intent(this, FileChooser.class);
		intent.putExtra("CHOOSER_TYPE", FILECHOOSER);
		if (isPlayer) {
			intent.putExtra("FILTER_TYPE", 1);
		} else if (isRecorder) {
			intent.putExtra("FILTER_TYPE", 2);
		} else if (isEditor) {
			intent.putExtra("FILTER_TYPE", 3);
		}

		startActivityForResult(intent, REQUEST_CODE);
	}

	private SharedPreferences getPrefs() {
//		if (isPlayer) {
//			return getSharedPreferences("PlayerPrefs", MODE_PRIVATE);
//		}
//		if (isRecorder) {
//			return getSharedPreferences("RecorderPrefs", MODE_PRIVATE);
//		}
//		if (isEditor) {
//			return getSharedPreferences("EditorPrefs", MODE_PRIVATE);
//		}
		return getSharedPreferences("GeneralPrefs", MODE_PRIVATE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(tag, "entered onActivityResult " + requestCode + resultCode);
		Bundle recExtras;
		if ((requestCode == REQUEST_CODE) && (resultCode == RESULT_OK)) {
			recExtras = data.getExtras();
			String result = recExtras.getString("selected_path");
			Log.d(tag, result);
			
			insertNewItem(result);
			startToolset(result);
			populate(mArAdapter, mRecentFileList);
		}

	}

	private void insertNewItem(String newItem){
		SharedPreferences prefs = getPrefs();
		Log.d(tag, "want to add new item "+newItem);
		Boolean alreadyInList = false;
		for (int i = 0; i < 5; i++) { // search if result is already in the list
			if(mRecentFileList.get(i).equals(newItem)){
				alreadyInList = true;
				break;
			} else{
				alreadyInList = false;
			}
		}
		if (!alreadyInList) {
			// if result not in list: move entries
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("listItem5", prefs.getString("listItem4", ""));
			editor.putString("listItem4", prefs.getString("listItem3", ""));
			editor.putString("listItem3", prefs.getString("listItem2", ""));
			editor.putString("listItem2", prefs.getString("listItem1", ""));
			editor.putString("listItem1", newItem);
			editor.commit();
		}
	}
	
	public void onClearClick(View v) {
		mArAdapter.clear();
		SharedPreferences prefs = getPrefs();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("listItem2", "");
		editor.putString("listItem3", "");
		editor.putString("listItem4", "");
		editor.putString("listItem5", "");
		editor.putString("listItem1", "");
		editor.commit();
		return;
	}

	public void onNewPresentationClick(View v) {
		Intent i = new Intent(this, FileChooser.class);
		emptyProject = true;
		i.putExtra("CHOOSER_TYPE", DIRCETORYCHOOSER);
		startActivityForResult(i, REQUEST_CODE);
	}

	private void startToolset(String file) {
		Intent intent;
		if (isRecorder) {
			intent = new Intent(this, Recorder.class);
			Log.d(tag, "starting Recorder");
		} else if (isPlayer) {
			intent = new Intent(this, Player.class);
			Log.d(tag, "starting Player");
		} else if (isEditor) {
			intent = new Intent(this, Editor.class);
			Log.d(tag, "starting Editor");
		} else {
			intent = new Intent(this, Player.class);
			Log.d(tag, "starting default Player");
		}
		intent.putExtra("selected_path", file);
		intent.putExtra("empty_project", emptyProject);
		emptyProject = false;
		startActivity(intent);
	}
}
