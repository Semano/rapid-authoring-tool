package dipl.dist.RapidAuthoringApp;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
/**
 * This is an extra class for the camera preview.   
 * @author Johannes Holtz
 *
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
		private SurfaceHolder mHolder;
		private Camera mCamera;
		public CameraPreview(Context contex, Camera cam) {
			super(contex);
			if(cam != null){
				mCamera = cam;
			}else{
				Log.d("CameraPreview", "given camera instance is null");
			}
			mHolder = getHolder();
			if (mHolder == null)Log.d("campreview", "holder is null");
			mHolder.addCallback(this);

			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);			//for compatibility
			mHolder.setSizeFromLayout();
		}
		
		public void surfaceCreated(SurfaceHolder holder) {
			// called when the view is completely created and the surface is usable
			if((holder != null)&&(mCamera != null)){
			try{
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
			}
			catch(IOException e)
			{
				Log.d("camerapreview","Error while setting up camera preview "+e.getMessage());
			}
			}
			else{
				Log.d(VIEW_LOG_TAG, "surface holder is missing on surface creation");
			}
			
		}
		
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			// If your preview can change or rotate, take care of those events here.
	        // Make sure to stop the preview before resizing or reformatting it.

	        if (mHolder.getSurface() == null){
	          // preview surface does not exist
	          return;
	        }
	        if(mCamera!=null){
	        	mCamera.stopPreview();
	        }
	        /*
	         * If mCamera is stopped here it causes a error in the media service
	         */
	        
	        // calculate best size for the preview
	        Camera.Parameters params = mCamera.getParameters();
			Camera.Size size = getBestPreviewSize(params, width, height);
	        
	        // start preview with new settings
	        try {
	        	if(size != null){ params.setPreviewSize(size.width, size.height);}
	            mCamera.setPreviewDisplay(holder);
	            mCamera.startPreview();
	            Log.d("CAMERA_PREVIEW", "set preview size");
	        } catch (Exception e){
	            Log.d("surfaceChanged", "Error starting camera preview: " + e.getMessage());
	        }
		}
		
		private Size getBestPreviewSize(Parameters params, int width, int height) {
			Camera.Size result = null; 

	        for (Camera.Size size : params.getSupportedPreviewSizes())
	        {// for all supported sizes 
	        	Log.d("CAMERA_PREVIEW", "size:width "+size.width+" size:height "+size.height);
	            if (size.width <= width && size.height <= height)
	            {// if supported width and height smaller than given and result not already set than set the supported size as result
	            	// result will be the minimum supported size
	                if (null == result)
	                result = size; 
	            else
	            {// supported size lager that given 
	                int resultDelta = width - result.width + height - result.height;//
	                int newDelta    = width - size.width   + height - size.height;

	                    if (newDelta < resultDelta)
	                result = size; 
	            }
	            } 
	        }
	        return result; 
		}
		
		
		public void surfaceDestroyed(SurfaceHolder holder) {
			if (mHolder.getSurface() == null){
	          // preview surface does not exist
	          return;
	        }
			mCamera.release();
	        mCamera = null;
		}
	}