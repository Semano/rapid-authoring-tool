/**
 * 
 */
package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This surface view implements a special View for video replay.
 * @author jh006
 * 
 */
public class VideoPlayerView extends SurfaceView implements OnPreparedListener, OnCompletionListener, SurfaceHolder.Callback,
		OnErrorListener {
	private static final String tag = "VIDEOPLAYER";
	private SurfaceHolder mHolder;
	private MediaPlayer mMediaPlayer;
	// mSecMediaPlayer;
	private Presentation mProject;
	private Toolset mParent;

	/**
	 * @param context
	 */
	public VideoPlayerView(Context context, Presentation pres) {
		super(context);

		mProject = pres;
		mHolder = getHolder();
		mHolder.addCallback(this);
		mParent = (Toolset) context;

		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setOnCompletionListener(this);
		mMediaPlayer.setOnPreparedListener(this);
		mMediaPlayer.setOnErrorListener(this);
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

		/*
		 * mSecMediaPlayer = new MediaPlayer(); mSecMediaPlayer.setOnCompletionListener(this); mSecMediaPlayer.setOnPreparedListener(this);
		 * mSecMediaPlayer.setOnErrorListener(this); mSecMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		 */

		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); // for compatibility
		mHolder.setSizeFromLayout();

		requestFocus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.media.MediaPlayer.OnCompletionListener#onCompletion(android.media.MediaPlayer)
	 */
	public void onCompletion(MediaPlayer mp) {
		Log.d(tag, "video playback completed");
		String path;
		if (mParent.nextElementIsNeighbour()) {
			if(mParent.isForward()){
				mParent.onNextBtnClick(null);
			}else{
				mParent.onPrevBtnClick(null);
			}
			path = mParent.getElementVideoPath();
			Log.d(tag, "next video will be "+path);
			try {
				mp.reset();
				mp.setDataSource(mProject.getProjectPath() + File.separator + path);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mp.prepareAsync();
		} else {
			Log.d(tag, "video sequence completed");
			mParent.onVideoComplete();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.media.MediaPlayer.OnPreparedListener#onPrepared(android.media.MediaPlayer)
	 */
	public void onPrepared(MediaPlayer mp) {
		mp.start();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

	}

	public void surfaceCreated(SurfaceHolder holder) {
		mMediaPlayer.setDisplay(holder);
		Log.d(tag, "created surface");
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	/**
	 * sets the datasource in order to play a video to the given path
	 * 
	 * @param relativePath
	 */
	public void play(String relativePath) {
		if (relativePath.equals(""))
			return;
		String path = mProject.getProjectPath() + File.separator;
		Log.d(tag, "preparing video source " + path + relativePath);
		try {
			mMediaPlayer.setDataSource(path + relativePath);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mMediaPlayer.prepareAsync();
	}

	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		mp.reset();
		mp.setDisplay(mHolder);
		if (mp.equals(mMediaPlayer)) {
			Log.d(tag, "first media recorder " + what + " " + extra);
		}
		return false;
	}

	public void stop() {
		mMediaPlayer.stop();
		mMediaPlayer.release();
		mMediaPlayer = null;
	}

}
