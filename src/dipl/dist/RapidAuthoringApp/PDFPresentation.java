/**
 * 
 */
package dipl.dist.RapidAuthoringApp;

import java.io.File;

import pdftron.Common.PDFNetException;
import pdftron.PDF.PDFDoc;
import pdftron.PDF.PDFDraw;
import pdftron.PDF.Page;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;

/**
 * This class derives from the {@link Presentation} class and is created if a PDF File is used as input.
 * @author Johannes Holtz
 * 
 */
public class PDFPresentation extends Presentation {

	private static final String tag = "PDFPRESENTATION";

	private PDFDoc mPDFDoc;
	private PDFDraw mPDFDraw;
	protected int mCurPageNum;

	public PDFPresentation(Context parent, String path, String projName) {
		super(parent, path, projName);

		mCurPageNum = 0;
		mCurSubPageNum = 0;
		mPageCount = 0;
		mPageToRender = -1;
		mSubPageToRender = -1;
		/**
		 * create a PDFDraw instance
		 */
		try {
			mPDFDraw = new PDFDraw();
		} catch (Exception e) {
			Log.d(tag, e.getMessage());
		}
		/**
		 * create the drawing thread, which will be started in surfaceCreated()
		 */
		//mDrawThread = new PDFDrawThread(mSurfaceview.getHolder());

		// TODO Auto-generated constructor stub
	}

	@Override
	public void buildSlides(Object pdf) {
		setDoc((PDFDoc) pdf);
		for (int i = 1; i <= mPageCount; i++) {
			Slide s = new Slide();
			s.setPageNum(i);
			s.setPagePath("Images" + File.separator + getImageName(i, 0));
			this.mSlides.add(s);
		}
		Log.d(tag, "added "+mPageCount+" Slides");
	}

	/**
	 * set the PDF file for drawing
	 */
	public void setDoc(PDFDoc doc) {
		mPDFDoc = doc;
		try {
			mPDFDoc.initSecurityHandler();
			mCurPageNum = 1;
			mCurSubPageNum = 0;
			mPageToRender = 1;
			mSubPageToRender = 0;
			mPageCount = mPDFDoc.getPageCount();
		} catch (PDFNetException e) {
			Log.d(tag, "An error occured in setDoc while initiating the PDF file");
		}

		/**
		 * create a new drawing thread; old thread should have been stopped in surfaceDestroyed().
		 */
		//mDrawThread = new PDFDrawThread(mSurfaceview.getHolder());

	}

	@Override
	public void startThread() {
		((PDFDrawThread) mDrawThread).setRunning(true);
		mDrawThread.start();
	}

	@Override
	public void stopThread() {
		boolean retry = true;
		while (retry) {
			try {
				// stop the running thread
				((PDFDrawThread) mDrawThread).setRunning(false);
				mDrawThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void createDrawThread() {
		if (mDrawThread == null) {
			mDrawThread = new PDFDrawThread();
		}
	}

	@Override
	public void changedLayout(int l, int t, int r, int b) {
		int width = r - l;
		int height = b - t;
		boolean preserve_aspect_ratio = true;
		try {
			mPDFDraw.setImageSize(width, height, preserve_aspect_ratio);
		} catch (PDFNetException e) {
		}
	}

	/**
	 * this is the background thread that uses PDFDraw to render PDF pages
	 */
	class PDFDrawThread extends Thread {
		private boolean mRun = false;

		public PDFDrawThread() {
		}

		public void setRunning(boolean running) {
			mRun = running;
		}

		@Override
		public void run() {
			while (mRun) {
				mPageInfoLock.lock();
				int page = mPageToRender;
				int subpage = mSubPageToRender;
				mPageInfoLock.unlock();

				if ((page > 0) || (subpage > 0)) {
					Log.d(tag, "page : " + Integer.toString(page) + " subpage : " + Integer.toString(subpage));
					if (page > 0) {
						// print a page
						Bitmap bmp = null;
						if (subpage == 0) {
							// render main page
							// only main pages need to be rendered
							// in case of subpage a blank canvas will be drawn
							bmp = doRender(page);
						}
						Canvas c = null;
						try {
							mPageInfoLock.lock();

							c = mHolder.lockCanvas();
							synchronized (mHolder) {
								if (bmp != null) {
									// if a mainpage was rendered draw it
									c.drawBitmap(bmp, 0, 0, null);
									Log.d(tag, "just drawed new main page");
								} else {
									// if current Slide is a subpage draw a blank canvas
									bmp = Bitmap.createBitmap(mSlideView.getWidth(), mSlideView.getHeight(), Bitmap.Config.ARGB_8888);
									for(int x = 0 ; x<mSlideView.getWidth()-1; x++){
										for(int y = 0 ; y<mSlideView.getHeight()-1; y++){
											bmp.setPixel(x, y, Color.LTGRAY);
										}
									}
									c.drawBitmap(bmp, 0, 0, null);
									Log.d("SLIDEVIEW", "just drawed new sub page");
								}
							}

							mPageToRender = -1;
							mSubPageToRender = -1;
							// save Image to sdcard/cache
							saveImage(bmp, page, subpage);
							Log.d(tag, "saved page");
						} finally {
							mPageInfoLock.unlock();
							if (c != null) {
								mHolder.unlockCanvasAndPost(c);
							}
						}
					}
				}
			}
		}

		/**
		 * use pdftron.PDF.PDFDraw to render a page
		 */
		private Bitmap doRender(int page_num) {
			try {
				Page pg = mPDFDoc.getPage(page_num);
				return mPDFDraw.getBitmap(pg);
			} catch (Exception e) {
			}
			return null;
		}
	}
}
