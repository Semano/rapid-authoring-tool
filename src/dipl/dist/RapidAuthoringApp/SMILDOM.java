package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import android.util.Log;
import android.widget.TextView;
import dipl.dist.RapidAuthoringApp.Slide.SlideElement;

/**
 * This class wraps the DOM used for the output of presentations.
 * @author Johannes Holtz
 *
 */
public class SMILDOM {
	private static final String tag = "DOM_PARSER";
	private Document mSMIL;
	private Element mHead;
	private Element mRootSeq;

	/**
	 * Constructor. Builds a new SMIL document with a standard layout setting and a &lt;body&gt; which contains an empty &lt;par&gt; tag.  
	 * @param width		width of the display layout to produce a matching layout.
	 * @param height	height of the display.
	 */
	public SMILDOM(int width, int height) {
		// using DOM parser
		try {
			DocumentBuilderFactory builderFac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = builderFac.newDocumentBuilder();
			mSMIL = docBuilder.newDocument();
			Element root = mSMIL.createElement("smil");
			// root element
			root.setAttribute("xmlns", "http://www.w3.org/ns/SMIL");
			root.setAttribute("version", "3.0");
			root.setAttribute("baseProfile", "Language");
			mSMIL.appendChild(root);
			// header with layout
			mHead = mSMIL.createElement("head");
			Element layout = mSMIL.createElement("layout");

			Element rootLayout = mSMIL.createElement("root-layout");
			rootLayout.setAttribute("width", Integer.toString(width));
			rootLayout.setAttribute("height", Integer.toString(height));
			rootLayout.setAttribute("backgroundColor", "black");
			layout.appendChild(rootLayout);

			Element regVid = mSMIL.createElement("region");
			regVid.setAttribute("xml:id", "Video");
			regVid.setAttribute("top", "80%");
			regVid.setAttribute("right", "80%");
			regVid.setAttribute("fit", "meet");
			layout.appendChild(regVid);

			Element regSlide = mSMIL.createElement("region");
			regSlide.setAttribute("xml:id", "Slide");
			regSlide.setAttribute("left", "20%");
			regSlide.setAttribute("bottom", "20%");
			regSlide.setAttribute("fit", "meet");
			layout.appendChild(regSlide);

			mHead.appendChild(layout);

			root.appendChild(mHead);

			// begin with body
			Element body = mSMIL.createElement("body");
			root.appendChild(body);
			mRootSeq = mSMIL.createElement("seq");
			body.appendChild(mRootSeq);
			Log.d(tag, "finished the head section in smile file");
		} catch (Exception e) {
			Log.d(tag, "error in DOM parser: " + e.getMessage());
		}
	}

	public NodeList getPars() {
		return mRootSeq.getChildNodes();
	}

	public void setDoc(Document d) {
		mSMIL = d;
		NodeList n;
		n = mSMIL.getElementsByTagName("head");
		mHead = (Element) n.item(0);
		n = mSMIL.getElementsByTagName("seq");
		mRootSeq = (Element) n.item(0);
	}

	public void appendParElement(Element par) {
		mRootSeq.appendChild(par);
	}

	public Element createElement(String name) {
		return mSMIL.createElement(name);
	}

	public Text createTextNode(String data) {
		return mSMIL.createTextNode(data);
	}

	/**
	 * transforms the dom to a string and saves it in a project file
	 * @param path	path where the output file will be located and named
	 */
	public void safeDOM(String path) {
		// save DOM to file
		try {

			TransformerFactory transFac = TransformerFactory.newInstance();
			if (transFac == null)
				Log.d(tag, "transformer factory is null");
			Transformer trans = transFac.newTransformer();
			if (trans == null) {
				Log.d(tag, "transformer is null");
			}
			DOMSource source = new DOMSource(mSMIL);
			StreamResult result = new StreamResult(new FileOutputStream(path));
			trans.transform(source, result);

			Log.d(tag, "saved SMIL file ");

		} catch (FileNotFoundException efnf) {
			Log.d(tag, "could not open file output stream with path: " + path + "\n" + efnf.getMessage());
		} catch (TransformerConfigurationException etc) {
			Log.d(tag, "transfromer config exception:" + etc.getMessage());
		} catch (TransformerException et) {
			Log.d(tag, "transfromer exception:" + et.getMessage());
		} catch (NullPointerException npe) {
			Log.d(tag, "NPE: " + npe.getMessage());
			npe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	/**
	 * converts the given project structure to a SMIL-compatible DOM. Afterward is will be saved as a tmp file.  
	 * @param project	the project structure which will be converted
	 */
	public void safeProjectToSmil(Presentation project) {
		// transform Presentation to DOM
		Element newRootSeq = mSMIL.createElement("seq");
		for (int i = 0; i < project.getNumberOfMainpages(); i++) {
			Slide s = project.getMainSlide(i);
			Element par = createElementFromSlide(s);
			newRootSeq.appendChild(par);
			if (s.hasSubPages()) {
				for (int sub = 0; sub < s.getSizeofSubpages(); sub++) {
					Element sp = createElementFromSlide(s.getSubpage(sub));
					newRootSeq.appendChild(sp);
				}
			}
		}
		mRootSeq.getParentNode().replaceChild(newRootSeq, mRootSeq);
		mRootSeq = (Element) newRootSeq.getParentNode();
		safeDOM(project.getProjectPath() + File.separator + project.getProjectName() + ".tmp.smil");
	}
	
	/**
	 * removes all child elements from the &lt;seq&gt; tag
	 */
	public void clearSEQ(){
		NodeList n = mRootSeq.getChildNodes();
		for(int i =0;i<n.getLength();i++){
			removeAll(n.item(i));
			mRootSeq.removeChild(n.item(i));
		}
	}

	/**
	 * recursively removes all child nodes and the node itself
	 * @param node the node which should be removed
	 */
	private void removeAll(Node node){
		NodeList n = node.getChildNodes();
		for(int i = 0; i<n.getLength();i++){
			if(n.item(i).hasChildNodes()) //edit to remove children of children
            {
              removeAll(n.item(i));
              node.removeChild(n.item(i));
            }
            else
              node.removeChild(n.item(i));
		}
	}
	
	/**
	 * creates an &lt;par&gt; element from a given slide
	 * @param s	the slide which should be converted
	 * @return	an element which the corresponding children
	 */
	private Element createElementFromSlide(Slide s) {
		Element par = mSMIL.createElement("par");

		Element img = mSMIL.createElement("img");
		img.setAttribute("region", "Slide");
		img.setAttribute("src", s.getPagePath());
		img.setAttribute("dur", "1s");
		par.appendChild(img);

		for (SlideElement se : s.getElements()) {
			Element elem;
			switch (se.getType()) {
			case TEXT: {
				elem = mSMIL.createElement("smiltext");
				elem.appendChild(mSMIL.createTextNode(((TextView) se.getView()).getText().toString()));
				par.appendChild(elem);
				break;
			}
			case IMG: {
				elem = mSMIL.createElement("img");
				Log.d(tag, "img transformation not yet implemented");
				break;
			}
			default: {
				elem = mSMIL.createElement("img");
			}
			}
			elem.setAttribute("region", "Slide");
			elem.setAttribute("left", Integer.toString(se.getX()));
			elem.setAttribute("top", Integer.toString(se.getY()));
		}
		return par;
	}
}
