package dipl.dist.RapidAuthoringApp;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import dipl.dist.RapidAuthoringApp.R.id;

/**
 * This activity is a simple File Chooser dialog with a customized list view adapter.
 * @author Johannes Holtz
 *
 */
public class FileChooser extends Activity implements OnItemClickListener {

	private static final String tag = "FILE_CHOOSER";
	private ListView mListView;
	private FileListAdapter mArAdapter;
	private File directory;
	private boolean mExternalStorageAvailable = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	private String selectedFile;
	private boolean dirChooser; // false: pdf, smil and directories | true: only directories
	private int filterType;

	/**
	 * fills the adapter which is used to display the files. 
	 * @param dir	the {@link File} object which represents the directory that is displayed
	 */
	private void populateAdapter(File dir) {
		if (mExternalStorageAvailable) {
			directory = dir;
			if (directory.isDirectory()) {
				String[] filelist = directory.list();
				if (directory.list() == null) {
					Log.d("FileChooser", "dir is not a directory");
				}
				Log.d("FileChooser", "number of entrys in filelist is " + filelist.length);
				if (!dirChooser) {
					switch (filterType) {
					case 1:
					case 2:
					default: {// Player and Recorder
						for (String file : filelist) {
							if (file.endsWith(".pdf") | file.endsWith(".PDF")) {
								// mArAdapter.add(" | PDF File  | " + file);
								mArAdapter.add(file);
							} else if (file.endsWith(".smil") | file.endsWith(".SMIL")) {
								// mArAdapter.add(" | SMIL File | " + file);
								mArAdapter.add(file);
							} else if (new File(directory.getAbsolutePath() + File.separator + file).isDirectory()) {
								// mArAdapter.add(" | DIRECTORY | " + file);
								mArAdapter.add(file);
							}
						}
						break;
					}
					case 3: {// Editor
						for (String file : filelist) {
							if (file.endsWith(".smil") | file.endsWith(".SMIL")
									| new File(directory.getAbsolutePath() + File.separator + file).isDirectory()) {
								mArAdapter.add(file);
								Log.d("FileChooser", file);
							}
						}
						break;
					}
					}

				} else {
					for (String file : filelist) {
						if (new File(directory.getAbsoluteFile() + File.separator + file).isDirectory()) {
							mArAdapter.add(file);
						}
					}
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Button btnUp = (Button) findViewById(R.id.btn_up);
		btnUp.setEnabled(true);

		if (mExternalStorageAvailable) {
			directory = Environment.getExternalStorageDirectory();
		}
		
		mListView = (ListView) findViewById(id.browserlist);
		mArAdapter = new FileListAdapter(this, R.layout.extended_listitem_filechooser, R.id.tv_praefix, directory.getAbsolutePath());
		mListView.setAdapter(mArAdapter);
		mListView.setOnItemClickListener(this);
		
		Intent i = getIntent();
		dirChooser = i.getBooleanExtra("CHOOSER_TYPE", false);
		if (dirChooser) {
			Log.d(tag, "directory chooser");
			Button btnSelect = (Button) findViewById(R.id.btn_select);
			btnSelect.setEnabled(true);
		}
		filterType = i.getIntExtra("FILTER_TYPE", -1);

		populateAdapter(directory);
	}

	/**
	 * Opens an {@link AlertDialog} to insert a project name. called after click on the "select" button. 
	 * @param v the View which caused the call. should be the select button
	 */
	public void onSelectClick(View v) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Project Name");
		alert.setMessage("Please enter a project name for your presentation");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();

				selectedFile = directory.getAbsolutePath() + File.separator + value + File.separator + value +".tmp.smil";
				Log.d(tag, "selected project folder and name is " + selectedFile);
				Intent intent = getIntent();
				intent.putExtra("selected_path", selectedFile);
				setResult(RESULT_OK, intent);
				finish();
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}
	
	/**
	 * Changes the current directory to one step above. Causes a a refill of the adapter.
	 * @param v the view which caused the call. should be the up button
	 */
	public void onUpClick(View v) {
		if (directory.getParentFile().canRead()) {
			directory = directory.getParentFile();
			mArAdapter.clear();
			mArAdapter.setDirectory(directory.getAbsolutePath());
			populateAdapter(directory);
			return;
		} else {
			return;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_chooser);
	}

	/**
	 * Called if an item from the adapter view is clicked. If it is a directory the directory will be opened and the adapter will be refilled. If it is a file the path will be send as intent result.
	 */
	public void onItemClick(AdapterView<?> arg0, android.view.View arg1, int position, long arg3) {
		Log.d("FileChooser", "entered onItemClick callback");
		selectedFile = mArAdapter.getItem(position);
		if (selectedFile == "..") {
			if (directory.getParentFile().canRead()) {
				directory = directory.getParentFile();
				mArAdapter.clear();
				populateAdapter(directory);
				return;
			} else {
				return;
			}
		} else if (selectedFile == ".") {
			return;
		}

		File file = new File(directory.getAbsolutePath() + File.separator + selectedFile);
		if (file.isDirectory()) {
			mArAdapter.clear();
			directory = file;
			mArAdapter.setDirectory(file.getAbsolutePath());
			populateAdapter(directory);
		} else {
			Intent intent = getIntent();
			intent.putExtra("selected_path", file.getAbsolutePath());
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_file_chooser, menu);
		return true;
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
