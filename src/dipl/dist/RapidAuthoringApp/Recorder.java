package dipl.dist.RapidAuthoringApp;

import android.widget.RadioGroup;
import android.widget.ToggleButton;
import dipl.dist.RapidAuthoringApp.R.id;
/**
 * This class implements the tools exclusively used for the behavior of the recorder.
 * @author Johannes Holtz
 *
 */
public class Recorder extends Toolset {
	@Override
	protected void setLayout() {
		setContentView(R.layout.activity_recorder);
	}

	@Override
	protected void activateToolBtns() {
		// Tools
		ToggleButton tool = (ToggleButton) findViewById(id.BtnRecToolTextBox);
		if (mRecorderStatus == RecorderStatus.PLAYBACK) {
			tool.setEnabled(false);
		} else {
			tool.setEnabled(true);
		}
		tool = (ToggleButton) findViewById(id.BtnRecToolFreeForm);
		if (mRecorderStatus == RecorderStatus.PLAYBACK) {
			tool.setEnabled(false);
		} else {
			tool.setEnabled(true);
		}
	}

	@Override
	protected void setupToolBtns() {
		mSlideBtnGroup = (RadioGroup) findViewById(id.BtnGrpRecorderTools);
		mSlideBtnGroup.setOnCheckedChangeListener(this);

	}

	@Override
	protected void initInstanceSpecific() {
		// TODO Auto-generated method stub
		
	}

}
