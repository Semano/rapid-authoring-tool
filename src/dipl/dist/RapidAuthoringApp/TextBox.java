package dipl.dist.RapidAuthoringApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This implementation of a {@link TextView} adds an onTouch listener and opens a string input dialog on click. 
 * @author jh006
 *
 */
public class TextBox extends TextView implements OnTouchListener {
	Context mContext;
	Toolset mParent;

	/**
	 * creates a clickable {@link TextView} with transparent background black font in size 18.
	 * @param parent	the parent {@link Toolset} implementation
	 * @param text		the text of the TextView
	 */
	public TextBox(Toolset parent, String text) {
		super(parent);
		mContext = parent;
		mParent = parent;
		setBackgroundColor(Color.TRANSPARENT);
		setText(text);
		setTextSize(18);
		setTextColor(Color.BLACK);
		setClickable(true);
		setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			openTextInputDialog((TextBox) v);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Called on click on a {@link TextView}. Opens an {@link AlertDialog} which can override the text.
	 * 
	 * @param textbox
	 *            the {@link TextView} of which the text will be changed
	 */
	public void openTextInputDialog(final TextBox textbox) {

		AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

		alert.setTitle("New Text Box");
		alert.setMessage("Please enter a text");

		// Set an EditText view to get user input
		final EditText input = new EditText(mContext);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				textbox.setText(value);
				mParent.onTextChanged(textbox);
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}
}
