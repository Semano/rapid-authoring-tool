/**
 * 
 */
package dipl.dist.RapidAuthoringApp;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import android.view.View;

/**
 * This class represents the Slide which are part of the {@link Presentation} structure.
 * @author Johannes Holtz
 * 
 */
public class Slide {

	private int mPageNumer;
	private String mPagePath;
	private String mVideoPath;
	private long mStartTime;
	private long mDuration;
	private List<SlideElement> mElements;
	private List<Slide> mSubPages;
	private Slide mParent;

	/**
	 * This enumeration contains all types of {@link SlideElements}. Has to be extended if new SlideElements are implemented.
	 * @author jh006
	 *
	 */
	public enum SlideElementType {
		TEXT, IMG
	}

	/***
	 * This inner class is used to save the position of views which where placed over the Slideview. This enables the rebuilding of them
	 * after resume.
	 * 
	 * @author Johannes Holtz
	 * 
	 */
	public class SlideElement implements Comparator<SlideElement> {

		private int xOffset, yOffset;
		private View view;
		private SlideElementType type;
		private long begin;
		private long end;

		public SlideElement(int x, int y, View v, SlideElementType t, long be, long en) {
			xOffset = x;
			yOffset = y;
			view = v;
			type = t;
			begin = be;
			end = en;
		}

		public int compare(SlideElement lhs, SlideElement rhs) {
			if ((lhs.xOffset != rhs.xOffset) || (lhs.yOffset != rhs.yOffset) || (lhs.view != rhs.view)) {
				return -1;
			} else {
				return 0;
			}
		}

		public void setTranslation() {
			view.setTranslationX(xOffset);
			view.setTranslationY(yOffset);
		}

		public View getView() {
			return view;
		}

		public void setEnd(long time) {
			end = time;
		}
		
		public long getEnd(){
			return end;
		}

		public long getBegin() {
			return begin;
		}

		public int getX() {
			return xOffset;
		}

		public int getY() {
			return yOffset;
		}

		public SlideElementType getType() {
			return type;
		}

	}

	public Slide() {
		mPageNumer = 0;
		mPagePath = "";
		mVideoPath = "";
		mStartTime = 0;
		mDuration = 0;
		mElements = new LinkedList<SlideElement>();
		mSubPages = new LinkedList<Slide>();
		mParent = null;
	}

	public void setPageNum(int pageNum) {
		mPageNumer = pageNum;
	}

	public int getPageNumber() {
		return mPageNumer;
	}

	public void setPagePath(String path) {
		mPagePath = path;
	}

	public String getPagePath() {
		return mPagePath;
	}

	public void setParent(Slide s) {
		mParent = s;
	}

	public Slide getParent() {
		return mParent;
	}

	public void setVideoPath(String path) {
		mVideoPath = path;
	}

	public String getVideoPath() {
		return mVideoPath;
	}

	public void addSlideElement(SlideElement e) {
		mElements.add(e);
	}

	/***
	 * gets the subpage defined by its index in the list.
	 * 
	 * @param index
	 *            poisition in the subpage list
	 * @return returns the corresponding slide object if available, null otherwise
	 */
	public Slide getSubpage(int number) {
		try{
			return mSubPages.get(number);
		}catch(IndexOutOfBoundsException e){
			e.printStackTrace();
			return null;
		}
		
	}

	public int getSizeofSubpages() {
		return mSubPages.size();
	}

	public boolean isMainpage() {
		if (mParent == null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasSubPages() {
		if ((mSubPages != null) && !mSubPages.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public List<SlideElement> getElements() {
		return mElements;
	}

	public boolean hasElements() {
		if ((mElements != null) && !mElements.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	public void setDuartion(long time) {
		mDuration = time;
	}

	public long getDuration() {
		return mDuration;
	}

	public void setStartTime(long time) {
		mStartTime = time;
	}

	public long getStartTime() {
		return mStartTime;
	}

	public void addSubPage(Slide s) {
		if (mParent == null) {
			if (mSubPages == null) {
				mSubPages = new LinkedList<Slide>();
			}
			mSubPages.add(s);
		}
	}
	
	public void addSubPage(Slide s, int index) {
		if (mParent == null) {
			if (mSubPages == null) {
				mSubPages = new LinkedList<Slide>();
			}
			mSubPages.add(index, s);
		}
	}

}
