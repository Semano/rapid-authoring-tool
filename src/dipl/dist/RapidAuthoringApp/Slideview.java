package dipl.dist.RapidAuthoringApp;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This class implements the working area of the application. It manages the received touch and life cycle events.
 * 
 * @author Johannes Holtz
 * 
 */
public class Slideview extends SurfaceView implements SurfaceHolder.Callback {
	private Toolset mParent;
	private SurfaceHolder mHolder;
	private GestureDetector mGestureDetector;
	private static final String tag = "SLIDEVIEW";

	private SimpleOnGestureListener simpleOnGestureListener = new SimpleOnGestureListener() {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			Log.d(tag, "entered onFling");
			final int sensitivity = 50;

			if ((e1.getX() - e2.getX()) > sensitivity) {
				// swipe left
				if (mParent.isNxtBtnEnbld()) {
					Log.d(tag, "swipe left");
					mParent.onNextBtnClick(null);
				}
			} else if ((e2.getX() - e1.getX()) > sensitivity) {
				// swipe right
				if (mParent.isPrevBtnEbld()) {
					Log.d(tag, "swipe right");
					mParent.onPrevBtnClick(null);
				}
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}

		// walk-around cause else onFling() is not executed
		public boolean onDown(MotionEvent e) {
			return true;
		};
	};

	public Slideview(Context context, Toolset rec) {
		super(context);
		mParent = rec;

		this.setFocusable(true);
		this.setLongClickable(true);

		mHolder = getHolder();
		mHolder.addCallback(this);

		mGestureDetector = new GestureDetector(getContext(), simpleOnGestureListener);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (changed) {
			mParent.getProject().changedLayout(l, t, r, b);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mParent.isToolSelected()) {
			// is a tool is selected use position as cursor
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				mParent.useTool((int) event.getX(), (int) event.getY());
			}
			return true;
		} else {
			// if no tool is selected use swipe gesture
			return mGestureDetector.onTouchEvent(event);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(tag, "surface created");
		mParent.getProject().setHolder(holder);
		if (mParent.getCurSlide().isMainpage()) {
			mParent.getProject().setPageToRender(mParent.getCurSlide().getPageNumber(), 0);
		} else {
			mParent.getProject().setPageToRender(mParent.getCurSlide().getParent().getPageNumber(), mParent.getCurSlide().getPageNumber());
		}
		mParent.getProject().createDrawThread();
		mParent.getProject().startThread();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		mParent.getProject().stopThread();
	}

}
