package dipl.dist.RapidAuthoringApp;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import dipl.dist.RapidAuthoringApp.R.id;

public class FileChooserRecord extends Activity implements OnItemClickListener {
	private ListView mListView; 
	private ArrayAdapter<String> mArAdapter;
	String[] mRecentFileList = new String[5];
	private static final int REQUEST_CODE = 10;
	private static final String PREF_RECENT_USED = "RecentUsedFiles";
	
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_chooser_record);
        mListView = (ListView) findViewById(id.lv_recent_files);
        mArAdapter = new ArrayAdapter<String>(this, R.layout.listitem_filechooser);
        mListView.setAdapter(mArAdapter);
        mListView.setOnItemClickListener(this);
        
        SharedPreferences prefs = getSharedPreferences(PREF_RECENT_USED, MODE_PRIVATE);
    	SharedPreferences.Editor editor = prefs.edit();
    	editor.commit();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	mArAdapter.clear();
    	SharedPreferences prefs = getSharedPreferences(PREF_RECENT_USED, MODE_PRIVATE);
    	
    		mRecentFileList[0]=prefs.getString("listItem1","");
    		mRecentFileList[1]=prefs.getString("listItem2","");
    		mRecentFileList[2]=prefs.getString("listItem3","");
    		mRecentFileList[3]=prefs.getString("listItem4","");
    		mRecentFileList[4]=prefs.getString("listItem5","");
    		purgeDuplicats(mRecentFileList);
    		for(String s : mRecentFileList){
    			int index = s.lastIndexOf(File.separator) == -1 ? 0 : s.lastIndexOf(File.separator); 
    			if(index == 0){
    				mArAdapter.add(s);
    			}else{
    				mArAdapter.add(s.substring(index+1,s.length()));
    			}
    		}
    }

    private void purgeDuplicats(String[] rfl) {
		for( int i=0 ; i< rfl.length ;i++){
			// for every entry in the list 
			for(int j=0; j<rfl.length; j++){
				// compare every other entry
				if((j!=i) & !rfl[i].equals("") & rfl[i].equals(rfl[j])){
					// ignore itself 
					// compare only if not empty
					// if equal set entry empty
					rfl[j]="";
				}
			}
		}
		
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
    	String selectedFile = mRecentFileList[position];
    	if(selectedFile.equals(""))return;
		startRecorder(selectedFile);
		Log.d("FileChooserRecord", "selected File: "+selectedFile);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_file_chooser_record, menu);
        return true;
    }
    
    public void onClickChoose(View v){
        Intent intent = new Intent(this, FileChooser.class);
        startActivityForResult(intent, REQUEST_CODE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	Log.d("FileChooserRecord", "entered onActivityResult "+ requestCode + resultCode);
    	Bundle recExtras;
    	if((requestCode == REQUEST_CODE)&&(resultCode == RESULT_OK)){
    		recExtras = data.getExtras();
    		String result = recExtras.getString("selected_path");
    		Log.d("FileChooserRecord", result);
    		
    		SharedPreferences prefs = getSharedPreferences(PREF_RECENT_USED, MODE_PRIVATE);
    		Boolean alreadyInList = false;
    		for(int i = 0;i<5;i++){ // search if result is already in the list
    			if(prefs.getString("listItem"+Integer.toString(i+1), "").equals(result)){
    				alreadyInList = true;
    				break;
    			}else alreadyInList = false;
    			
    		}
    		if(!alreadyInList){
    			//if result not in list: rotate entries
	    		SharedPreferences.Editor editor = prefs.edit();    		
	    		editor.putString("listItem2", prefs.getString("listItem1", ""));
	    		editor.putString("listItem3", prefs.getString("listItem2", ""));
	    		editor.putString("listItem4", prefs.getString("listItem3", ""));
	    		editor.putString("listItem5", prefs.getString("listItem4", ""));
	    		editor.putString("listItem1", result);    		
	    		editor.commit();
    		}
    		startRecorder(result);
    	}
    	
    }
    
    public void onClearClick(View v){
    	mArAdapter.clear();
    	SharedPreferences prefs = getSharedPreferences(PREF_RECENT_USED, MODE_PRIVATE);
    	SharedPreferences.Editor editor = prefs.edit();    		
		editor.putString("listItem2", "");
		editor.putString("listItem3", "");
		editor.putString("listItem4", "");
		editor.putString("listItem5", "");
		editor.putString("listItem1", "");    		
		editor.commit();
    	return;
    }
    
    private void startRecorder(String file){
    	Intent intent = new Intent(this, Recorder.class);
		intent.putExtra("selected_path", file);
		startActivity(intent);
    }
}
