/**
 * 
 */
package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;
import dipl.dist.RapidAuthoringApp.Slide.SlideElement;
import dipl.dist.RapidAuthoringApp.Slide.SlideElementType;

/**
 * This class is a specialization of the presentation which is build from a given SMIL document 
 * @author Johannes Holtz
 * 
 */
public class SMILPresentation extends Presentation {
	private static String tag = "SMILE_PRESENTATION";
	private ArrayList<PendingSlide> pendingSlides = new ArrayList<SMILPresentation.PendingSlide>();

	class PendingSlide {
		Slide slide;
		int mainnum;
		boolean deleted;

		public PendingSlide(Slide s, int num) {
			slide = s;
			mainnum = num;
			deleted = false;
		}

		public void delete() {
			deleted = true;
		}

		@Override
		public boolean equals(Object o) {
			if (o.getClass().getName() == this.getClass().getName()) {
				PendingSlide ps = (PendingSlide) o;
				if (ps.mainnum == this.mainnum) {
					if (ps.slide.getPageNumber() == this.slide.getPageNumber()) {
						if (ps.slide.isMainpage() && this.slide.isMainpage()) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	/**
	 * @param parent
	 * @param path
	 */
	public SMILPresentation(Context parent, String path, String proName) {
		super(parent, path, proName);
		// TODO Auto-generated constructor stub
	}

	/***
	 * 
	 * This method builds a {@link Presentation} structure from a given SMIL DOM
	 * 
	 * @param o
	 *            The {@link SMILDOM} object
	 */
	@Override
	public void buildSlides(Object o) {
		SMILDOM dom = (SMILDOM) o;

		Log.d(tag, "SMIL Doc detected");

		this.mSlides = new LinkedList<Slide>();

		NodeList pars = dom.getPars();
		if (pars.getLength() < 1) {
			Slide slide = new Slide();
			slide.setPageNum(1);
			slide.setParent(null);
			slide.setPagePath("Images" + File.separator + getImageName(1, 0));
			mSlides.add(slide);
		}
		int pagenum = 1;
		Log.d(tag, " numer of elements in <par> :" + pars.getLength());
		for (int i = 0; i < pars.getLength(); i++) {
			Log.d(tag, "processing slide " + pagenum);
			Slide s = new Slide();

			NodeList parChilds = pars.item(i).getChildNodes();
			boolean found = false;
			boolean isSubpage = false;
			int tmp = 0;
			for (int j = 0; j < parChilds.getLength(); j++) {
				Log.d(tag, "processing element " + j);
				if (parChilds.item(j).getNodeType() == Node.ELEMENT_NODE) {
					Log.d(tag, "Element node " + parChilds.item(j).getNodeName());
					if (parChilds.item(j).getNodeName().equals("img")) {
						Log.d(tag, "img detected");
						if (!found) {
							String pagepath = parChilds.item(j).getAttributes().getNamedItem("src").getNodeValue();
							s.setPagePath(pagepath);
							int start = pagepath.lastIndexOf("_");
							if (start < 2) {
								found = false;
								break;
							}
							int end = pagepath.lastIndexOf(".");
							if (end <= start) {
								found = false;
								break;
							}
							String subpage = pagepath.substring(start + 1, end);
							end = start;
							start = pagepath.lastIndexOf(File.separator);
							String page = pagepath.substring(start + 3, end);
							int subnumber = Integer.valueOf(subpage);
							int mainnumber = Integer.valueOf(page);
							Log.d(tag, "read page number: " + mainnumber + " and subpagenumber: " + subnumber);
							if (!isDouble(mainnumber, subnumber)) {
								Log.d(tag, "no doppelgaenger found");
								if (subnumber > 0) {
									// subpage found
									s.setPageNum(subnumber);
									tmp = mainnumber;
									isSubpage = true;
									found = true;
								} else if (subnumber == 0) {
									// mainpage found
									s.setPageNum(mainnumber);
									isSubpage = false;
									found = true;
								} else {
									found = false;
								}
							} else {
								Log.d(tag, "entry was found twice");
								break;
							}
						} else {
							// not yet implemented
						}
					} else if (parChilds.item(j).getNodeName().equals("video")) {
						Log.d(tag, "video detected");
						s.setVideoPath(parChilds.item(j).getAttributes().getNamedItem("src").getNodeValue());
					}
					if (parChilds.item(j).getNodeName().equals("smiltext")) {
						Log.d(tag, "text detected");
						TextBox textbox = new TextBox((Toolset) mParent, parChilds.item(j).getTextContent());

						int y = Integer.valueOf(parChilds.item(j).getAttributes().getNamedItem("top").getNodeValue());
						int x = Integer.valueOf(parChilds.item(j).getAttributes().getNamedItem("left").getNodeValue());
						long begin = 0;
						String attribvalue;
						if (parChilds.item(j).getAttributes().getNamedItem("begin") != null) {
							attribvalue = parChilds.item(j).getAttributes().getNamedItem("begin").getNodeValue();
							begin = Long.parseLong(attribvalue.substring(0, attribvalue.length() - 2));
						}
						long end = -1;
						if (parChilds.item(j).getAttributes().getNamedItem("end") != null) {
							attribvalue = parChilds.item(j).getAttributes().getNamedItem("end").getNodeValue();
							end = Long.parseLong(attribvalue.substring(0, attribvalue.length() - 2));
						}
						SlideElement se = s.new SlideElement(x, y, textbox, SlideElementType.TEXT, begin, end);
						s.addSlideElement(se);
					}
				}
			}
			// slide object completed
			// adding it in the presentation
			if (found) {
				if (isSubpage) {
					if (includeSub(s, tmp)) {
						Log.d(tag, "added Slide to Project (subpage) ");
					} else {
						Log.d(tag, "added element to Pending Slide List");
					}
					isSubpage = false;
				} else {
					includeMain(s);
					checkPending();
					pagenum++;
					Log.d(tag, "added Slide to Project");
				}
			}
		}

		includePending();

		mPageCount = mSlides.size();
		Log.d(tag, "amount of main pages: " + mPageCount);
		Log.d(tag, "amount of Slides which could not be inculed is " + pendingSlides.size());
		mDrawThread = new SMILDrawThread();
	}

	private void includePending() {
		for (int i = 0; i < pendingSlides.size(); i++) {
			PendingSlide pending = pendingSlides.get(i);
			Slide s = new Slide();
			s.setPageNum(pending.mainnum);
			s.setPagePath("Images" + File.separator + getImageName(pending.mainnum, 0));
			s.setParent(null);
			TextView tv = new TextView(mParent);
			tv.setText("This slide was inserted for the correct function of the App. The slide containes no video and is not related to your presentation");
			tv.setTextColor(Color.BLACK);
			tv.setBackgroundColor(Color.TRANSPARENT);
			SlideElement se = s.new SlideElement(10, 10, tv, SlideElementType.TEXT, 0, -1);
			includeMain(s);
			checkPending();
		}

	}

	private void checkPending() {
		for (int i = 0; i < pendingSlides.size(); i++) {
			if (includeSub(pendingSlides.get(i).slide, pendingSlides.get(i).mainnum)) {
				pendingSlides.remove(i);
				i = -1;
			}
		}
	}

	private boolean includeMain(Slide newSlide) {
		if (mSlides.isEmpty()) {
			mSlides.add(newSlide);
			return true;
		} else {
			for (Slide s : mSlides) {
				if (s.getPageNumber() > newSlide.getPageNumber()) {
					try {
						mSlides.add(getIndex(s), newSlide);
						return true;
					} catch (Exception e) {
						Log.d(tag, "Could not add mainslide to end of Slidelist");
						e.printStackTrace();
					}
				}
			}
			mSlides.add(newSlide);
			return true;
		}
	}

	private void addToPending(PendingSlide ps){
		for(PendingSlide p : pendingSlides){
			if(p.equals(ps)){
				Log.d(tag, "NOT adding to pending slides list");
				return;
			}
		}
		pendingSlides.add(ps);
	}

	private boolean includeSub(Slide newSlide, int mainnumber) {
		if (mSlides.isEmpty()) {
			addToPending(new PendingSlide(newSlide, mainnumber));
			return false;
		} else {
			for (Slide s : mSlides) {
				if (mainnumber == s.getPageNumber()) {
					// parent page found
					newSlide.setParent(s);
					for (int i = 0; i < s.getSizeofSubpages(); i++) {
						if (s.getSubpage(i).getPageNumber() > newSlide.getPageNumber()) {
							s.addSubPage(newSlide, i);
							return true;
						}
					}
					s.addSubPage(newSlide);
					return true;
				}
			}
			// no matching parent found
			addToPending(new PendingSlide(newSlide, mainnumber));
			return false;
		}
	}

	private boolean isDouble(int mainnumber, int subnumber) {
		for (final Slide s : mSlides) {
			if (s.getPageNumber() == mainnumber) {
				if (subnumber == 0) {
					return true;
				} else {
					if (s.hasSubPages()) {
						for (int i = 0; i < s.getSizeofSubpages(); i++) {
							if (subnumber == s.getSubpage(i).getPageNumber()) {
								return true;
							}
						}
					} else {
						return false;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void createDrawThread() {
		if (mDrawThread == null) {
			mDrawThread = new SMILDrawThread();
		}
	}

	@Override
	public void startThread() {
		((SMILDrawThread) mDrawThread).setRunning(true);
		mDrawThread.start();
	}

	@Override
	public void stopThread() {
		boolean retry = true;
		while (retry) {
			try {
				// stop the running thread
				((SMILDrawThread) mDrawThread).setRunning(false);
				mDrawThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}

	}

	@Override
	public void changedLayout(int l, int t, int r, int b) {
		// TODO Auto-generated method stub

	}

	private class SMILDrawThread extends Thread {
		private boolean mRun;

		public void setRunning(boolean b) {
			mRun = b;
		}

		public SMILDrawThread() {
		}

		@Override
		public void run() {
			while (mRun) {
				mPageInfoLock.lock();
				int page = mPageToRender;
				int subpage = mSubPageToRender;
				mPageInfoLock.unlock();

				if ((page > 0) && (subpage >= 0)) {
					Log.d(tag, "draw thread found uncached SMIL page ... ");
					Bitmap bmp = Bitmap.createBitmap(mSlideView.getWidth(), mSlideView.getHeight(), Bitmap.Config.ARGB_8888);
					for (int x = 0; x < bmp.getWidth(); x++) {
						for (int y = 0; y < bmp.getHeight(); y++) {
							bmp.setPixel(x, y, Color.LTGRAY);
						}
					}
					Canvas c = null;
					try {
						c = mHolder.lockCanvas();
						synchronized (mHolder) {
							c.drawBitmap(bmp, 0, 0, null);
						}
						saveImage(bmp, page, subpage);
						Log.d(tag, "saved page");
					} finally {
						if (c != null) {
							mHolder.unlockCanvasAndPost(c);
						}
						mPageInfoLock.lock();
						mPageToRender = -1;
						mSubPageToRender = -1;
						mPageInfoLock.unlock();
					}
				}
			}
		}
	}
}
