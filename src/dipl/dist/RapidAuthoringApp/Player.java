package dipl.dist.RapidAuthoringApp;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
/**
 * This class implements the Controller layer exclusively used for playing presentations
 * @author Johannes Holtz
 *
 */
public class Player extends Toolset implements OnItemClickListener{

	private ListView mSlideOverview;
	private ArrayAdapter<String> mArAdapter;
	
	@Override
	protected void setLayout() {
		setContentView(R.layout.activity_player);
		mSlideOverview = (ListView) findViewById(R.id.playerOverView);
		mArAdapter = new ArrayAdapter<String>(this, R.layout.listitem_filechooser);
		mSlideOverview.setAdapter(mArAdapter);
		mSlideOverview.setOnItemClickListener(this);
	}

	@Override
	protected void activateToolBtns() {
		if(mRecorderStatus == RecorderStatus.PLAYBACK){
			Button btn = (Button) findViewById(R.id.BtnRecOpNext);
			btn.setEnabled(false);
			btn = (Button)findViewById(R.id.BtnRecOpPrev);
			btn.setEnabled(false);
		}
	}
	
	@Override
	protected void setupToolBtns() {	
	}

	@Override
	protected void initInstanceSpecific() {
		updateListView();
	}

	private void updateListView(){
		mArAdapter.clear();
		Slide s = mProject.getMainSlide(0);
		String label;
		for (int i= 1 ; i<=mProject.getNumberOfSlides(); i++) {
			if(s.isMainpage()){
				label = s.getPageNumber()+". -";
			}else{
				label = s.getParent().getPageNumber()+"."+s.getPageNumber()+ " -";
			}
			mArAdapter.add(label);
			s = mProject.getNext(s);
			if(s == null){
				break;
			}
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Log.d(tag, " List Item selected");
		// remove added views from layout
		for (Slide.SlideElement se : mCurSlide.getElements()) {
			mSlideviewLayout.removeView(se.getView());
		}
		mCurSlide = mProject.getSlide(position);

		mProject.setPageToRender(mCurSlide);
		// add the elements associated with this page to the layout
		redrawElements();

		activateButtons();
	}
}
