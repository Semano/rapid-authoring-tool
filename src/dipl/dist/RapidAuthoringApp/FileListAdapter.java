package dipl.dist.RapidAuthoringApp;

import java.io.File;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
/**
 * Custom Class for the display of the {@link ListView} entries and the corresponding file type. 
 * @author Johannes Holtz
 *
 */
public class FileListAdapter extends ArrayAdapter<String>{
	private final Context mContext;
	private final int mLayout;
	private final int mViewId;
	private String mDirectory;

	public FileListAdapter(Context context, int resource, int textViewResourceId, String directory) {
		super(context, resource, textViewResourceId);
		mContext = context;
		mLayout = resource;
		mViewId = textViewResourceId;
		mDirectory = directory;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(mLayout, parent, false);
	    TextView praefix = (TextView) rowView.findViewById(mViewId);
	    TextView suffix = (TextView) rowView.findViewById(R.id.tv_suffix);
	    String entry = getItem(position);
	    praefix.setText(entry);
	    if(mDirectory != null){
		    if(new File(mDirectory + File.separator + entry).isDirectory()){
		    	suffix.setText(" | DIRECTORY");
		    }else if(entry.endsWith(".pdf")|entry.endsWith(".PDF")){
		    	suffix.setText(" | PDF File");
		    }
		    else if(entry.endsWith(".smil")|entry.endsWith(".SMIL")){
		    	suffix.setText(" | SMIL File");
		    }
	    }else{
	    	if(entry.endsWith(".pdf")|entry.endsWith(".PDF")){
	    		suffix.setText(" | PDF File");
	    	}else if(entry.endsWith(".smil")|entry.endsWith(".SMIL")){
		    	suffix.setText(" | SMIL File");
		    }
	    }
		return rowView;
	}
	
	public void setDirectory(String path){
		mDirectory = path;
	}
}