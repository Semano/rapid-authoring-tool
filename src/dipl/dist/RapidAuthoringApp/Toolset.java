package dipl.dist.RapidAuthoringApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import pdftron.PDF.PDFDoc;
import pdftron.PDF.PDFNet;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import dipl.dist.RapidAuthoringApp.R.id;
import dipl.dist.RapidAuthoringApp.Slide.SlideElement;
import dipl.dist.RapidAuthoringApp.Slide.SlideElementType;

/**
 * This is the activity for the record feature of the application. It handles the camera preview, the canvas view and the tool buttons.
 * 
 * @author Johannes Holtz
 * 
 */
public abstract class Toolset extends Activity implements RadioGroup.OnCheckedChangeListener {

	protected static enum RecorderStatus {
		SILENT, RECORDING, PLAYBACK
	}

	public static enum SelectedBtn {
		TEXTBOX, FREEFORM, NONE
	}

	private static final String mOutputPath = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_MOVIES;
	private static int mCamSelect;

	/**
	 * This method creates a new camera handle. It selects the first front facing camera.
	 * 
	 * @return a camera handle to the first found front facing camera
	 */
	public static Camera getCameraInstance() {
		Camera c = null;
		int cnum = 0;
		mCamSelect = 0;
		Camera.CameraInfo caminfo = new CameraInfo();
		try {
			cnum = Camera.getNumberOfCameras();
			Log.d("getCameraInstance", "amount of camera devices" + String.valueOf(cnum));
			for (int i = 0; i < cnum; i++) {
				Camera.getCameraInfo(i, caminfo);
				if (caminfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
					mCamSelect = i;
					break;
				}
			}
			c = Camera.open(mCamSelect); // attempt to get a Camera instance
			// if no front facing camera was found mCamSelect is still 0 and the
			// first camera device should be used
		} catch (Exception e) {
			Log.d("getCameraInstance", "FATAL camera could not be opened");
			// Camera is not available (in use or does not exist)
		}
		if (c == null)
			Log.d("getCameraInstance", "no camera returned");
		return c; // returns null if camera is unavailable
	}

	private Camera mCamera;
	private CameraPreview mCameraPreview;
	private MediaRecorder mMediaRecorder;
	private Slideview mSlideview;
	protected RelativeLayout mSlideviewLayout;
	private FrameLayout mPreviewLayout;
	protected RadioGroup mSlideBtnGroup;
	private String mSelectedFile;
	private File mCurVidFile = null;
	private PDFDoc mPDF = null;
	private CountDownTimer mMSTimer = null;
	private long mRecTime = 0;
	private boolean mMSTimerEnabled = false;
	private VideoPlayerView mVideoView;
	protected static final String tag = "TOOLSET";
	private final ReentrantLock mTimerLock = new ReentrantLock();;
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
	private SelectedBtn btnStatus = SelectedBtn.NONE;
	protected RecorderStatus mRecorderStatus = RecorderStatus.SILENT;
	protected Presentation mProject;
	protected Slide mCurSlide;
	private SMILDOM mSmilDoc;
	private Element mCurDOMElement;
	protected boolean mPageInc = false; // true increment; false decrement;
	private boolean mIsPdf = false;
	private boolean mIsForward = true;
	private boolean mIsDirty = false;

	public boolean isPrevBtnEbld() {
		return ((Button) findViewById(id.BtnRecOpPrev)).isEnabled();
	}

	public boolean isNxtBtnEnbld() {
		return ((Button) findViewById(id.BtnRecOpNext)).isEnabled();
	}

	/***
	 * enabled or disables the buttons on the UI
	 */
	protected void activateButtons() {
		// next and prev. Buttons
		Button nxt = (Button) findViewById(id.BtnRecOpNext);
		Button prv = (Button) findViewById(id.BtnRecOpPrev);
		Log.d(tag, "activating Navigation Buttons");

		if (nxt != null && prv != null) {
			if (mCurSlide.isMainpage()) {
				if (mCurSlide.hasSubPages()) {
					nxt.setEnabled(true);
				} else {
					// if Slide has no subpages: disable if it is the last slide
					nxt.setEnabled(mProject.getIndex(mCurSlide) < mProject.getNumberOfMainpages() - 1);
				}
				// because the first slide is always a Mainpage just check if it is the first
				prv.setEnabled(mProject.getIndex(mCurSlide) > 0);
			} else {
				// if Slide is a Subpage
				if (mProject.getIndex(mCurSlide) < mCurSlide.getParent().getSizeofSubpages() - 1) {
					// if Slide is not the last subpage of his mainpage
					nxt.setEnabled(true);
				} else {
					// if Slide is the last subpage
					if (mProject.getIndex(mCurSlide.getParent()) < mProject.getNumberOfMainpages() - 1) {
						// if there are further pages
						nxt.setEnabled(true);
					} else {
						// if Slide is the very last Page
						nxt.setEnabled(false);
					}
				}
				// because every subpage has at least a main page the prev btn is always enabled
				prv.setEnabled(true);
			}
		}

		// Video Buttons
		ToggleButton vid = (ToggleButton) findViewById(id.BtnRecOpStartStopp);
		if (vid != null) {
			if (mRecorderStatus == RecorderStatus.RECORDING) {
				vid.setEnabled(true);
			} else if (mRecorderStatus == RecorderStatus.PLAYBACK) {
				vid.setEnabled(false);
			} else {
				if (!hasVideo(mCurSlide)) {
					vid.setEnabled(true);
				} else {
					vid.setEnabled(false);
				}
			}
		}

		// Playback Button
		ToggleButton play = (ToggleButton) findViewById(id.BtnRecOpPlayPause);
		if (play != null) {
			if (mRecorderStatus != RecorderStatus.RECORDING) {
				play.setEnabled(!mCurSlide.getVideoPath().isEmpty());
			} else {
				play.setEnabled(false);
			}
		}

		activateToolBtns();

	}

	private boolean hasVideo(Slide slide) {
		if (slide.getVideoPath().equals("")) {
			NodeList nl = mSmilDoc.getPars();
			if (nl.getLength() > 0) {
				for (Node n = nl.item(0); n != null; n = n.getNextSibling()) {
					if (n.getNodeType() == Node.ELEMENT_NODE) {
						int page, subpage;
						if (slide.isMainpage()) {
							page = slide.getPageNumber();
							subpage = 0;
						} else {
							page = slide.getParent().getPageNumber();
							subpage = slide.getPageNumber();
						}
						if (isNodeToSlide(page, subpage, n)) {
							String vid = getFirstSource(n, false);
							if (vid.equals("")) {
								return false;
							} else {
								return true;
							}
						}
					}
				}
				return false;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	protected abstract void activateToolBtns();

	/***
	 * changes the status of the state machine and modifies the Presentation structure and the DOM. It also controls the start and stop of
	 * the recorder and player objects.
	 * 
	 * @param newStatus
	 *            the new status of the state machine
	 */
	private void changeRecorderStatus(RecorderStatus newStatus) {
		switch (mRecorderStatus) {
		case RECORDING: {
			if (newStatus == RecorderStatus.SILENT) {
				// STOP recording

				// 1. stop video recording
				stopRecording();
				// 2. modify project structure
				mTimerLock.lock();
				mCurSlide.setDuartion(mRecTime);
				for (Slide.SlideElement se : mCurSlide.getElements()) {
					se.setEnd(mRecTime);
				}
				mTimerLock.unlock();
				// 3. modify DOM
				// video tag
				NodeList nl = mCurDOMElement.getElementsByTagName("video");
				Element vid;
				if (nl.getLength() > 1) {
					Log.d(tag, "more that one video tag found. take first");
				}
				vid = (Element) nl.item(0);
				vid.setAttribute("dur", Long.toString(mCurSlide.getDuration()) + "ms");
				// img tag
				nl = mCurDOMElement.getElementsByTagName("img");
				Element img;
				if (nl.getLength() > 1) {
					Log.d(tag, "more that one img tag found. take first");
				}
				img = (Element) nl.item(0);
				img.setAttribute("dur", Long.toString(mCurSlide.getDuration()) + "ms");

				// Slide elements
				for (SlideElement se : mCurSlide.getElements()) {
					Element text = mSmilDoc.createElement("smiltext");
					text.setAttribute("begin", Long.toString(se.getBegin()) + "ms");
					text.setAttribute("end", Long.toString(se.getEnd()) + "ms");
					text.setAttribute("top", Integer.toString(se.getY()));
					text.setAttribute("left", Integer.toString(se.getX()));
					text.appendChild(mSmilDoc.createTextNode(((TextView) se.getView()).getText().toString()));
					if (mCurDOMElement == null) {
						mCurDOMElement = mSmilDoc.createElement("par");
					}
					mCurDOMElement.appendChild(text);
				}

				// append par tag to seq
				mSmilDoc.appendParElement(mCurDOMElement);
				mCurDOMElement = null;
				mIsDirty = true;
				// safe smil file to disk
				if (mIsDirty) {
					mSmilDoc.safeDOM(mProject.getProjectPath() + File.separator + mProject.getProjectName() + "_"
							+ df.format(Calendar.getInstance().getTime()) + ".proj.smil");
					mIsDirty = false;
				}

				mRecorderStatus = RecorderStatus.SILENT;
			} else if (newStatus == RecorderStatus.RECORDING) {
				// changed view while recording (switched page)
				// new elements will be processed at another place
				// 1. stop video recording
				stopRecording();
				// 2. modify project structure
				mTimerLock.lock();
				mCurSlide.setDuartion(mRecTime);
				for (Slide.SlideElement se : mCurSlide.getElements()) {
					se.setEnd(mRecTime);
				}
				mTimerLock.unlock();
				// 3. modify DOM
				// video tag
				NodeList nl = mCurDOMElement.getElementsByTagName("video");
				Element vid;
				if (nl.getLength() > 1) {
					Log.d(tag, "more that one video tag found. take first");
				}
				vid = (Element) nl.item(0);
				vid.setAttribute("dur", Long.toString(mCurSlide.getDuration()) + "ms");
				// img tag
				nl = mCurDOMElement.getElementsByTagName("img");
				Element img;
				if (nl.getLength() > 1) {
					Log.d(tag, "more that one img tag found. take first");
				}
				img = (Element) nl.item(0);
				img.setAttribute("dur", Long.toString(mCurSlide.getDuration()) + "ms");

				// Slide elements
				for (SlideElement se : mCurSlide.getElements()) {
					Element text = mSmilDoc.createElement("smiltext");
					text.setAttribute("begin", Long.toString(se.getBegin()) + "ms");
					text.setAttribute("end", Long.toString(se.getEnd()) + "ms");
					text.setAttribute("top", Integer.toString(se.getY()));
					text.setAttribute("left", Integer.toString(se.getX()));
					text.appendChild(mSmilDoc.createTextNode(((TextView) se.getView()).getText().toString()));
					if (mCurDOMElement == null) {
						mCurDOMElement = mSmilDoc.createElement("par");
					}
					mCurDOMElement.appendChild(text);
				}

				// append par tag to seq tag
				mSmilDoc.appendParElement(mCurDOMElement);
				// 4. start video recording again
				startRecording();
				// 5. determine new Slide
				updateCurSlide();
				// 6. modify project structure
				mCurSlide.setVideoPath("Videos" + File.separator + mCurVidFile.getName());
				mCurSlide.setStartTime(0);
				mCurSlide.setDuartion(-1);
				// 6. modify DOM
				// create new par section
				Element newElement = mSmilDoc.createElement("par");
				mCurDOMElement = newElement;
				// add video tag
				Element newVideo = mSmilDoc.createElement("video");
				vid = newVideo;
				vid.setAttribute("src", mCurSlide.getVideoPath());
				vid.setAttribute("type", "video/mpeg");
				vid.setAttribute("begin", Long.toString(mCurSlide.getStartTime()) + "ms");
				vid.setAttribute("region", "Video");
				mCurDOMElement.appendChild(vid);
				// add img tag
				Element newImage = mSmilDoc.createElement("img");
				img = newImage;
				img.setAttribute("src", mCurSlide.getPagePath());
				img.setAttribute("begin", Long.toString(mCurSlide.getStartTime()) + "ms");
				img.setAttribute("region", "Slide");
				mCurDOMElement.appendChild(img);

				mIsDirty = true;
			}
			break;
		}
		case SILENT: {
			switch (newStatus) {
			case RECORDING: {
				// START recording
				// 1. start video recording
				startRecording();
				// 2. modify project structure if necessary
				mCurSlide.setVideoPath("Videos" + File.separator + mCurVidFile.getName());
				mCurSlide.setStartTime(0);
				mCurSlide.setDuartion(-1);
				// 3. modify DOM
				mCurDOMElement = mSmilDoc.createElement("par");
				Log.d(tag, "created new PAR element");
				mSmilDoc.clearSEQ();

				// use mCurDOMElement insead of building new elements
				Element vid, img;
				vid = mSmilDoc.createElement("video");
				img = mSmilDoc.createElement("img");

				vid.setAttribute("src", mCurSlide.getVideoPath());
				vid.setAttribute("type", "video/mpeg");
				vid.setAttribute("begin", Long.toString(mCurSlide.getStartTime()) + "ms");
				vid.setAttribute("region", "Video");
				mCurDOMElement.appendChild(vid);

				img.setAttribute("src", mCurSlide.getPagePath());
				img.setAttribute("begin", Long.toString(mCurSlide.getStartTime()) + "ms");
				img.setAttribute("region", "Slide");
				mCurDOMElement.appendChild(img);

				mIsDirty = true;
				mRecorderStatus = RecorderStatus.RECORDING;
				break;
			}
			case PLAYBACK: {// START playback
				// no need to change ViewState for playback
				mRecorderStatus = RecorderStatus.PLAYBACK;
				mCurDOMElement = getElementfromSlide();
				break;
			}
			case SILENT: {// switch page
				updateCurSlide();
				updateCurDomElement();

				break;

			}
			default: {
			}
			}
			break;
		}
		case PLAYBACK: {
			if (newStatus == RecorderStatus.SILENT) {// STOP playback
				// only action which can change the status back to SILENT
				// no need to change the ViewState
				mRecorderStatus = RecorderStatus.SILENT;
			} else if (newStatus == RecorderStatus.PLAYBACK) {
				updateCurSlide();
				updateCurDomElement();
			}
			break;
		}
		default: {

		}
		}
	}

	private void updateCurDomElement() {
		Node el = mCurDOMElement;
		if (el != null) {
			while ((el = el.getNextSibling()) != null) {
				if ((el.getNodeType() == Node.ELEMENT_NODE) && (el.getNodeName().equals("par"))) {
					Log.d(tag, "CurDOM Element setto next silbing");
					break;
				}
			}
			mCurDOMElement = (Element) el;
		}
	}

	/**
	 * This method checks if the device provides a camera which can be used by the application.
	 * 
	 * @param context
	 *            the context from which the package manager will be called
	 * @return true if the package manager has a camera feature, false else
	 */
	private boolean checkCameraHardware(Context context) {
		boolean ret = true;
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			ret = true;
		} else {
			ret = false;
		}
		return ret;
	}

	public Slide getCurSlide() {
		return mCurSlide;
	}

	public Presentation getProject() {
		return mProject;
	}

	public Slideview getSlideview() {
		return mSlideview;
	}

	/*
	 * public String getProjectPath(){ return mOutputPath + File.separator + mSelectedFile; }
	 */

	/***
	 * handles the toggle events, called operations, from the recorder and player buttons
	 * 
	 * @param checked
	 *            the checked value of the toggle button
	 * @param view
	 *            the view object of the toggle button which raised the event
	 */
	private void handleOperation(boolean checked, View view) {
		switch (view.getId()) {
		case R.id.BtnRecOpStartStopp:
			handleRecorder(checked);
			break;
		case R.id.BtnRecOpPlayPause:
			handlePlayer(checked);
			break;
		}
	}

	/***
	 * handles the toggle event of the player button
	 * 
	 * @param checked
	 *            the checked value after the click
	 */
	private void handlePlayer(boolean checked) {
		if (checked) {
			// disable preview
			if (mRecorderStatus != RecorderStatus.PLAYBACK) {
				mPreviewLayout = (FrameLayout) findViewById(id.cameraPreview);
				mPreviewLayout.removeAllViews();
				mCameraPreview = null;
				Log.d(tag, "killed camera preview");
			}
			changeRecorderStatus(RecorderStatus.PLAYBACK);

			// create VideoView
			mVideoView = new VideoPlayerView(this, mProject);
			mPreviewLayout.addView(mVideoView);
			Log.d(tag, "created video view");

			// get path of the video
			Element element = getElementfromSlide();
			String relativePath = getFirstSource(element, false);
			Log.d(tag, "relative path is " + relativePath);
			// start playing
			mVideoView.play(relativePath);
		} else {
			onVideoComplete();
		}
	}

	/***
	 * handles the toggle event of the recorder button
	 * 
	 * @param checked
	 *            the checked value after the click
	 */
	private void handleRecorder(boolean checked) {
		if (checked) {
			if (mRecorderStatus == RecorderStatus.SILENT) {
				changeRecorderStatus(RecorderStatus.RECORDING);
			} else {
				Log.d(tag, "wrong internal State: " + RecorderStatus.SILENT + " expected, got " + mRecorderStatus);
			}
		} else {// if toggle button unchecked
			if (mRecorderStatus == RecorderStatus.RECORDING) {
				changeRecorderStatus(RecorderStatus.SILENT);
			} else {
				Log.d(tag, "wrong internal state: " + RecorderStatus.RECORDING + " expected, got " + mRecorderStatus);
			}
		}
	}

	/**
	 * handles the toggle event on one of the tool buttons in the tool button radio group. New Tools have to be included here. If a button
	 * is enabled this function sets a flag which can be checked at the next touch event.
	 * 
	 * @param checked
	 *            the checked value of the button after the click
	 * @param view
	 *            the view object of the tobble button
	 */
	private void handleToolSelection(boolean checked, View view) {
		if (checked) {
			// buttons states ok , now set the status right
			switch (view.getId()) {
			case R.id.BtnRecToolTextBox: {
				btnStatus = SelectedBtn.TEXTBOX;
				Log.d(tag, "changed Button Status to TEXTBOX");
				break;
			}
			case R.id.BtnRecToolFreeForm: {
				btnStatus = SelectedBtn.FREEFORM;
				Log.d(tag, "changed Button Status to FREEFROM");
				break;
			}
			default: {
				btnStatus = SelectedBtn.NONE;
				Log.d(tag, "changed Button Status to NONE");
				break;
			}
			}
		} else {
			btnStatus = SelectedBtn.NONE;
		}
	}

	/***
	 * Initialization of the App. It sets system flags, receives and interpret intents, builds an initial DOM and Presentation object.
	 */
	private void init() {
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		Intent mIntent = getIntent();
		Log.d(tag, "got intent");

		mSelectedFile = mIntent.getStringExtra("selected_path");
		String projectPath = "";
		String projectName = "";
		if (mSelectedFile.equals("")) {
			Log.d(tag, "got empty String as selected file path");
		} else {

			projectPath = mSelectedFile.substring(0, mSelectedFile.lastIndexOf(File.separator));
			projectName = mSelectedFile.substring(mSelectedFile.lastIndexOf(File.separator) + 1);
			// if (mIntent.getBooleanExtra("empty_project", false)) {
			int end = projectName.indexOf("_");
			if (end <= 0) {
				projectName = projectName.substring(0, projectName.indexOf("."));
			} else {
				projectName = projectName.substring(0, end);
			}
			// }
		}
		Log.d(tag, "set selected presentation name: " + projectName);

		// create SMIL DOM - filled through xml or through app
		Point size = new Point();
		Display d = getWindowManager().getDefaultDisplay();
		size.x = d.getWidth();
		size.y = d.getHeight();
		mSmilDoc = new SMILDOM(size.x, size.y);
		// PDF Processor initialization
		if (mSelectedFile.endsWith(".pdf") | mSelectedFile.endsWith(".PDF")) {
			Log.d(tag, "pdf detected");
			mIsPdf = true;
			try {
				PDFNet.initialize(this);
				FileInputStream is = new FileInputStream(mSelectedFile);
				mPDF = new PDFDoc(is);
			} catch (Exception e) {
			}
			Log.d(tag, "finished pdf initialization");
			mProject = new PDFPresentation(this, mOutputPath + File.separator + projectName, projectName);
			Log.d(tag, "created new pdf presentation instance");

			// SMIL initialization
		} else if (mSelectedFile.endsWith(".SMIL") | mSelectedFile.endsWith(".smil")) {
			Log.d(tag, "detected SMIL");
			mIsPdf = false;

			mProject = new SMILPresentation(this, projectPath, projectName);
			Log.d(tag, "created SMILDOM and SMIL Presentation instances");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				InputSource is = new InputSource(new FileInputStream(mSelectedFile));
				mSmilDoc.setDoc(db.parse(is)); // parse XML file to DOM
				mCurDOMElement = (Element) mSmilDoc.getPars().item(0);
				Log.d(tag, "parsed smil file and set smil dom");
			} catch (Exception e) {
				Log.d(tag, "error on loading xml : " + e.getMessage());
				e.printStackTrace();
			}
		} else {
			mProject = new SMILPresentation(this, projectPath, projectName);

			mIsPdf = false;
		}
		// create Project Folder if not existing
		File projectFolder = new File(mProject.getProjectPath());
		projectFolder.mkdirs();
		Log.d(tag, "created project folder");

		// Timer initialization
		mMSTimer = new CountDownTimer(100, 10) {

			@Override
			public void onFinish() {
				if (mMSTimerEnabled) {
					this.start();
				}
			}

			@Override
			public void onTick(long millisUntilFinished) {
				mTimerLock.lock();
				mRecTime += 10;
				mTimerLock.unlock();
			}
		};
	}

	/**
	 * Implements the radio button functionality of the toggle buttons.
	 */
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		Log.d(tag, "checkedID = " + checkedId);
		boolean selectionCleared = checkedId == -1 ? true : false;

		for (int i = 0; i < group.getChildCount(); i++) {
			if (selectionCleared) {
				// if selection is cleared then clear button status
				((ToggleButton) group.getChildAt(i)).setChecked(false);
			} else {
				// if button is turned on then set this button on and all other
				// off
				((ToggleButton) group.getChildAt(i)).setChecked(((ToggleButton) group.getChildAt(i)).getId() == checkedId);
			}
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLayout();
		init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_recorder, menu);
		return true;
	}

	/**
	 * Implementation of the 'new Slide' button. creates a new Slide in the Presentation and shows it.
	 * 
	 * @param v
	 *            the view which caused the call ( not used)
	 */
	public void onNewSlide(View v) {
		// remove added views from layout
		for (Slide.SlideElement se : mCurSlide.getElements()) {
			mSlideviewLayout.removeView(se.getView());
		}
		mPageInc = true;
		Slide s = new Slide();
		if (mCurSlide.isMainpage()) {
			s.setPageNum(1);
			s.setParent(mCurSlide);
			mCurSlide.addSubPage(s);
		} else {
			// if this slide is a subpage
			s.setPageNum(mCurSlide.getPageNumber() + 1);
			s.setParent(mCurSlide.getParent());
			Slide next = mProject.getNext(mCurSlide);
			if (next == null) {
				mCurSlide.getParent().addSubPage(s);
			} else {
				if (next.isMainpage()) {
					mCurSlide.getParent().addSubPage(s);
				} else {
					mCurSlide.getParent().addSubPage(s, mProject.getIndex(mCurSlide) + 1);
					int prevnum = next.getPageNumber();
					while ((next != null) && (!next.isMainpage())) {
						if (next.getPageNumber() == prevnum) {
							prevnum++;
							next.setPageNum(prevnum);
							String newimgpath = "Images" + File.separator
									+ mProject.getImageName(next.getParent().getPageNumber(), prevnum);
							File img = new File(mProject.mProjectPath + File.separator + next.getPagePath());
							img.renameTo(new File(mProject.mProjectPath + File.separator + newimgpath));
							next.setPagePath(newimgpath);
						}
						next = mProject.getNext(next);
					}
				}
			}
		}
		s.setPagePath("Images" + File.separator + mProject.getImageName(s.getParent().getPageNumber(), s.getPageNumber()));

		changeRecorderStatus(mRecorderStatus);

		// new slide is subpage. render subpage
		mProject.setPageToRender(s.getParent().getPageNumber(), s.getPageNumber());
		Log.d(tag, "incremented sub page counter in order to add an new slide");
		activateButtons();
	}

	/**
	 * getter of the selected button state
	 * 
	 * @return false if no tool is selected, true otherwise
	 */
	public boolean isToolSelected() {
		return btnStatus == SelectedBtn.NONE ? false : true;
	}

	/**
	 * Implementation of the 'next slide' button. Shows next slide.
	 * 
	 * @param v
	 */
	public void onNextBtnClick(View v) {
		Log.d(tag, "entered onNextBtnClick()");
		// remove added views from layout
		for (Slide.SlideElement se : mCurSlide.getElements()) {
			mSlideviewLayout.removeView(se.getView());
		}
		mPageInc = true;
		changeRecorderStatus(mRecorderStatus);

		if (mCurSlide.isMainpage()) {
			mProject.setPageToRender(mCurSlide.getPageNumber(), 0);
		} else {
			mProject.setPageToRender(mCurSlide.getParent().getPageNumber(), mCurSlide.getPageNumber());
		}
		// add the elements associated with this page to the layout
		redrawElements();

		activateButtons();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		// remove added views from layout
		for (Slide.SlideElement se : mCurSlide.getElements()) {
			mSlideviewLayout.removeView(se.getView());
		}

		if (mMediaRecorder != null) {
			mMediaRecorder.stop();
			releaseMediaRecorder();
			releaseCamera();
		}
		String path = mProject.getProjectPath() + File.separator + mProject.getProjectName();

		mSmilDoc.safeProjectToSmil(mProject);

		// if there were videos recorded: safe existing DOM
		if (mIsDirty) {
			mSmilDoc.safeDOM(path + "_" + df.format(Calendar.getInstance().getTime()) + ".proj.smil");
			mIsDirty = false;
		}

		super.onPause();
	}

	/**
	 * Implementation of the 'previous slide' button. Shows previous slide.
	 * 
	 * @param v
	 */
	public void onPrevBtnClick(View v) {
		// remove added views from layout
		for (Slide.SlideElement se : mCurSlide.getElements()) {
			mSlideviewLayout.removeView(se.getView());
		}
		mPageInc = false;
		changeRecorderStatus(mRecorderStatus);

		if (mCurSlide.isMainpage()) {
			mProject.setPageToRender(mCurSlide.getPageNumber(), 0);
		} else {
			mProject.setPageToRender(mCurSlide.getParent().getPageNumber(), mCurSlide.getPageNumber());
		}

		// add the elements associated with this page to the layout
		redrawElements();
		activateButtons();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// 1. set up camera preview
		if (checkCameraHardware(this)) {
			mCamera = getCameraInstance();
			mCameraPreview = new CameraPreview(this, mCamera);
			FrameLayout preview = (FrameLayout) findViewById(id.cameraPreview);
			preview.addView(mCameraPreview);
		} else {
			Log.d("Recorder", "camera check returned false");
		}

		// 2. setup Presentation and current slide
		if (!mIsPdf) {
			mProject.buildSlides(mSmilDoc);
		} else {
			mProject.buildSlides(mPDF);
		}
		Log.d(tag, "builded slides from source");

		if (mProject.hasSlides()) {
			mCurSlide = mProject.getMainSlide(0);
		} else {
			Log.d(tag, "Project Instance has no slides");
			mCurSlide = new Slide();
			mCurSlide.setPageNum(1);
			mCurSlide.setPagePath("Images" + File.separator + mProject.getImageName(1, 0));
			mCurSlide.setParent(null);
		}

		// 3. set up slideview
		mSlideviewLayout = (RelativeLayout) findViewById(id.slideview);
		mSlideview = new Slideview(this, this);
		mSlideviewLayout.addView(mSlideview);
		mProject.setSurfaceView(mSlideview);

		Log.d(tag, "starting element redraw");
		redrawElements();

		setupToolBtns();
		// 5. set up recorder toolbar
		activateButtons();
		Log.d(tag, "activated Buttons");
		// 6. setup camera
		mCamera.lock();
		Log.d(tag, "locked Camera to App");

		initInstanceSpecific();
	}

	/**
	 * Initialization of the instance specific buttons and data.
	 */
	protected abstract void initInstanceSpecific();

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		/*
		 * no longer needed cause of fixes screen orientation outState.putSerializable("SessionState", (Serializable) mViewStates);
		 * outState.putSerializable("CurrentViewState", mCurViewState);
		 */
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onStop() {
		mCamera.release();
		super.onStop();
	}

	/***
	 * This callback method checks the status of the toggled Button and sets radio group status correct. After this it changes the tool
	 * status for the slide tools or manages the video recording.
	 * 
	 * @param view
	 *            The toggled Button
	 */
	public void onToggle(View view) {
		boolean checked = ((ToggleButton) view).isChecked();
		if (!checked) {
			// Button is NOT checked
			((RadioGroup) view.getParent()).clearCheck();
		} else {
			// Button is checked
			((RadioGroup) view.getParent()).check(view.getId());
		}

		if (((RadioGroup) view.getParent()).getId() == R.id.BtnGrpRecorderTools) {
			// toggled Button is part of the slide tool bar
			handleToolSelection(checked, view);
		} else { // the toggled Button is part of the project operation tool bar
			handleOperation(checked, view);
		}
		activateButtons();
	}

	/**
	 * Called after finishing the last video of a row. finishes the replay and returns to start state.
	 */
	public void onVideoComplete() {
		changeRecorderStatus(RecorderStatus.SILENT);
		mVideoView.stop();
		mPreviewLayout.removeView(mVideoView);
		mVideoView = null;
		mPreviewLayout.removeAllViews();
		mCamera = getCameraInstance();
		mCameraPreview = new CameraPreview(this, mCamera);
		mPreviewLayout.addView(mCameraPreview);

		ToggleButton btn = (ToggleButton) findViewById(R.id.BtnRecOpPlayPause);
		btn.setChecked(false);
		activateButtons();
	}

	/**
	 * Preparation of the {@link MediaRecorder}. Sets video path format devices and view.
	 * 
	 * @return
	 */
	private boolean prepareVideoRecorder() {
		// ex: mCamera = getCameraInstance();
		mMediaRecorder = new MediaRecorder();
		Log.d(tag, "created MR");
		mMediaRecorder.reset();
		Log.d(tag, "MR reset");
		File videoFolder = new File(mProject.getProjectPath() + File.separator + "Videos");
		if (!videoFolder.exists() || !videoFolder.isDirectory()) {
			videoFolder.mkdirs();
		}
		mCurVidFile = new File(videoFolder.getAbsoluteFile(), df.format(Calendar.getInstance().getTime()) + ".mp4");
		Log.d(tag, "cur vid file");
		try {
			mCurVidFile.createNewFile();
			Log.d(tag, "created vid file");
		} catch (IOException e) {
			Log.d(tag, "could not create File. maybe allredy existing " + e.getMessage());
		}
		Camera.Parameters param = mCamera.getParameters();
		Camera.Size s = param.getSupportedPreviewSizes().get(0);

		// Step 1: Unlock and set camera to MediaRecorder
		mCamera.unlock();
		Log.d(tag, "cam unlock");
		mMediaRecorder.setCamera(mCamera);
		Log.d(tag, "MR setcamera");
		// Step 2: Set sources
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		Log.d(tag, "MR set audo source");
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		Log.d(tag, "MR set video source");
		// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)

		CamcorderProfile profile = CamcorderProfile.get(mCamSelect, CamcorderProfile.QUALITY_LOW);
		Log.d(tag, "set profile");
		if (profile == null) {
			Log.d(tag, "the camcorder profile instance is null");

			mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
			mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
			mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
		} else {
			mMediaRecorder.setProfile(profile);
		}
		Log.d(tag, "set profile");

		// Step 4: Set output file
		mMediaRecorder.setOutputFile(mCurVidFile.getAbsolutePath());
		Log.d(tag, "set output file");

		mMediaRecorder.setVideoSize(s.width, s.height);

		// Step 5: Set the preview output
		mMediaRecorder.setPreviewDisplay(mCameraPreview.getHolder().getSurface());// mSlideview.getHolder().getSurface());
		Log.d(tag, "set preview display");

		// Step 6: Prepare configured MediaRecorder
		try {
			mMediaRecorder.prepare();

			Log.d(tag, "MR prepare");
		} catch (IllegalStateException e) {
			Log.d(tag, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d(tag, "IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
		return true;
	}

	/**
	 * Removes all Views from the slide view and redraws the views from the current slide. Called after each navigation event.
	 */
	protected void redrawElements() {
		// remove all views
		if (mCurSlide != null) {
			if (mCurSlide.hasElements()) {
				for (Slide.SlideElement se : mCurSlide.getElements()) {
					View v = mSlideviewLayout.findViewById(se.getView().getId());
					if (v == null) {
						Log.d(tag, "view not found, adding it");
						// TODO check if element is out of view space
						// TODO if yes: show warning
						se.getView().setX(se.getX());
						se.getView().setY(se.getY());
						if (this instanceof Player) {
							se.getView().setClickable(false);
						}
						mSlideviewLayout.addView(se.getView());
					} else {
						Log.d(tag, "view not found");
						v.invalidate();
					}
				}
			} else {
				Log.d(tag, "no elements found on slide");
			}
		}
	}

	/**
	 * releases the camera and dismisses it
	 */
	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	/**
	 * releases the {@link MediaRecorder} and dismisses it. Locks camera to the App.
	 */
	private void releaseMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset(); // clear recorder configuration
			mMediaRecorder.release(); // release the recorder object
			mMediaRecorder = null;
			mCamera.lock(); // lock camera for later use
		}
	}

	/**
	 * Sets instance specific layout to the activity
	 */
	protected abstract void setLayout();

	/**
	 * Initiates the tool buttons for instance specific tools.
	 */
	protected abstract void setupToolBtns();

	/**
	 * Initiates the start of a recording process and also starts the parallel counter thread.
	 */
	private void startRecording() {
		Log.d(tag, "entering start recording");
		if (prepareVideoRecorder()) {
			try {
				// start recording
				mTimerLock.lock();
				Log.d(tag, "locked timer");
				mRecTime = 0;
				mMSTimerEnabled = true;
				Log.d(tag, "enabled timer");
				mTimerLock.unlock();
				Log.d(tag, "unlocked timer lock");
				mMediaRecorder.start();
				Log.d(tag, "start MR");
				mMSTimer.start();
				Log.d(tag, "timer start");
			} catch (IllegalStateException e) {
				Log.d(tag, "MR start() Illegal state Exc. " + e.getMessage());
			}
		}
	}

	/**
	 * Stops a the recording process.
	 */
	private void stopRecording() {
		try {
			// stop recording
			mMediaRecorder.stop();
			mMSTimerEnabled = false;
		} catch (IllegalStateException e) {
			Log.d(tag, "MR stop() Illegal state Exc. " + e.getMessage());
		} finally {
			releaseMediaRecorder();
		}
	}

	/**
	 * Sets the current slide pointer to the next of the previous slide
	 */
	private void updateCurSlide() {
		if (mCurSlide.isMainpage()) {
			Log.d(tag, " current  page is : " + mCurSlide.getPageNumber());
		} else {
			Log.d(tag, " current  page is : " + mCurSlide.getParent().getPageNumber() + " " + mCurSlide.getPageNumber());
		}
		Slide s;
		if (mPageInc) {
			s = mProject.getNext(mCurSlide);
		} else {
			s = mProject.getPrev(mCurSlide);
		}
		if (s != null) {
			mCurSlide = s;
		}
		if (mCurSlide.isMainpage()) {
			Log.d(tag, " new page is : " + mCurSlide.getPageNumber());
		} else {
			Log.d(tag, " new page is : " + mCurSlide.getParent().getPageNumber() + " " + mCurSlide.getPageNumber());
		}
	}

	/***
	 * This Method creates a EditView with transparent background at the given position relative to the 'slideview' layout.
	 * 
	 * @param x
	 *            X-position of the new created EditView
	 * @param y
	 *            Y-position of the new created EditView
	 */
	public void useTool(int x, int y) {
		switch (btnStatus) {
		case TEXTBOX: {

			String sampletext = "This is a new Text Box";
			final TextBox tv = new TextBox(this, sampletext);

			mTimerLock.lock();
			Slide.SlideElement se = mCurSlide.new SlideElement(x, y, tv, SlideElementType.TEXT, mRecTime, -1);
			mTimerLock.unlock();

			tv.setFocusableInTouchMode(true);
			tv.setInputType(InputType.TYPE_CLASS_TEXT);

			Log.d(tag, "pos x: " + x + " pos y: " + y);

			mSlideviewLayout.addView(tv);
			tv.setTranslationX(x);
			tv.setTranslationY(y);

			// modify Project structure
			mCurSlide.addSlideElement(se);
			break;
		}
		case FREEFORM: {
			break;
		}
		default: {
			Log.d(tag, "No btn Status matches");
		}
		}
	}

	public boolean nextElementIsNeighbour() {
		if (mCurDOMElement == null) {
			mCurDOMElement = getElementfromSlide();
		}
		if (mCurDOMElement == null) {
			return false;
		} else {
			Element nextElement = getNextElement(mCurDOMElement);
			if (nextElement == null) {
				return false;
			}
			Slide nextSlide = mProject.getNext(mCurSlide);
			int page, subpage;
			if (nextSlide != null) {
				if (nextSlide.isMainpage()) {
					page = nextSlide.getPageNumber();
					subpage = 0;
				} else {
					page = nextSlide.getParent().getPageNumber();
					subpage = nextSlide.getPageNumber();
				}
				Log.d(tag, "lookin if next element is neighbour; NEXT page " + page + " subpage " + subpage);
				if (isNodeToSlide(page, subpage, nextElement)) {
					mIsForward = true;
					return true;
				}
			}
			Slide prevSlide = mProject.getPrev(mCurSlide);
			if (prevSlide != null) {
				if (prevSlide.isMainpage()) {
					page = prevSlide.getPageNumber();
					subpage = 0;
				} else {
					page = prevSlide.getParent().getPageNumber();
					subpage = prevSlide.getPageNumber();
				}
				Log.d(tag, "lookin if next element is neighbour; PREV page " + page + " subpage " + subpage);
				if (isNodeToSlide(page, subpage, nextElement)) {
					mIsForward = false;
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	/**
	 * searches the DOM for the first appearance of a par element with an image which filename matches the page numbers of the slide. 
	 * @return the element if found; else null
	 */
	private Element getElementfromSlide() {
		NodeList pars = mSmilDoc.getPars();
		int page, subpage;
		if (mCurSlide.isMainpage()) {
			page = mCurSlide.getPageNumber();
			subpage = 0;
		} else {
			page = mCurSlide.getParent().getPageNumber();
			subpage = mCurSlide.getPageNumber();
		}
		// search each par tag and return the first element which matches the pagenumbers
		for (int i = 0; i <= pars.getLength(); i++) {
			if (isNodeToSlide(page, subpage, pars.item(i))) {
				return (Element) pars.item(i);
			}
		}
		return null;
	}

	/***
	 * checks if the {@link Node} has an img element with a src attribute which contains an image path where the page and the subpage are
	 * the same as the given
	 * 
	 * @param page
	 *            page number of the slide
	 * @param subpage
	 *            subpage number of the slide
	 * @param item
	 *            the par element to be checked
	 * @return true ifpage and subpage are both part of the src attribute of the img element, false otherwise
	 */
	private boolean isNodeToSlide(int page, int subpage, Node item) {

		boolean imgSrc = true;
		String path = getFirstSource(item, imgSrc);
		int start = path.lastIndexOf("_");
		if (start < 2) {
			return false;
		}
		int end = path.lastIndexOf(".");
		if (end <= start) {
			return false;
		}
		String subpageLiteral = path.substring(start + 1, end);
		end = start;
		start = path.lastIndexOf(File.separator);
		String pageLiteral = path.substring(start + 3, end);
		int subnumber = Integer.valueOf(subpageLiteral);
		int mainnumber = Integer.valueOf(pageLiteral);

		if ((mainnumber == page) && (subnumber == subpage)) {
			Log.d(tag, "is Node to Slide is ture");
			return true;
		}
		return false;
	}

	/**
	 * searches the given node children for an &lt;img&gt; tag and return the value of the src attribute
	 * 
	 * @param item
	 *            the &lt;par&gt; node of the dom which should be searched
	 * @param imgSrc
	 *            switch; function returns img path if true, video path otherwise
	 * 
	 * @return the value of the src attribute of the first &lt;img&gt; or &lt;video&gt; tag, empty if no src found
	 */
	private String getFirstSource(Node item, boolean imgSrc) {
		NodeList elements = item.getChildNodes();
		for (int i = 0; i < elements.getLength(); i++) {
			if (elements.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) elements.item(i);
				if (element.getNodeName().equals("video") && !imgSrc) {
					Log.d(tag, " video tag found and src is " + element.getAttribute("src"));
					return element.getAttribute("src");
				} else if (element.getNodeName().equals("img") && imgSrc) {
					Log.d(tag, " IMG tag found and src is " + element.getAttribute("src"));
					return element.getAttribute("src");
				}
			}
		}
		return "";
	}

	/***
	 * gets the value of the src attribute of the first element which matches the slide number and subpagenumber
	 * 
	 * @return the value of the src attribute of the &lt;img&gt; element of the next silbing of the current used <par> element, empty if no
	 *         src found
	 */
	public String getElementVideoPath() {
		if (mCurDOMElement == null) {
			mCurDOMElement = getElementfromSlide();
		}
		if (mCurDOMElement == null) {
			return "";
		} else {
			return getFirstSource(mCurDOMElement, false);
		}
	}

	/**
	 * Finds the next element in the &lt;seq&gt; tag and returns it.  
	 * @param current	the current element from wich the next is searched
	 * @return	the next sibling of the current element; null if there is no such node
	 */
	public Element getNextElement(Element current) {
		Node nextElement = current.getNextSibling();
		while (nextElement != null) {
			if (nextElement.getNodeType() == Node.ELEMENT_NODE) {
				if (nextElement.getNodeName().equals("par")) {
					break;
				}
			}
			nextElement = nextElement.getNextSibling();
		}
		return (Element) nextElement;
	}

	public boolean isForward() {
		boolean ret = mIsForward;
		mIsForward = true;
		return ret;
	}

	/**
	 * Called after TextView was changed. It searches the DOM for eac matching &lt;par&gt; element and changes the text of the matching &lt;smiltext&gt; element identified by the position of the element. 
	 * @param textbox the View which was changed
	 */
	public void onTextChanged(TextBox textbox) {
		// for each <par>
		if (mSmilDoc.getPars().getLength() != 0) {
			Node node;
			for (int i = 0; i < mSmilDoc.getPars().getLength(); i++) {
				node = mSmilDoc.getPars().item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// check if <par> matches current slide
					Element par = (Element) node;
					boolean nodeToSlide = false;
					if (mCurSlide.isMainpage()) {
						nodeToSlide = isNodeToSlide(mCurSlide.getPageNumber(), 0, par);
					} else {
						nodeToSlide = isNodeToSlide(mCurSlide.getParent().getPageNumber(), mCurSlide.getPageNumber(), par);
					}
					if (nodeToSlide) {
						for (Element e = (Element) par.getFirstChild(); e != null; e = (Element) e.getNextSibling()) {
							if (e.getNodeType() == Node.ELEMENT_NODE) {
								Log.d(tag, "element node name " + e.getNodeName());
								if (e.getNodeName().equals("smiltext")) {
									String attrib = e.getAttributes().getNamedItem("top").getNodeValue();
									int y = Integer.parseInt(attrib);
									Log.d(tag, "parsed top value is " + y);
									attrib = e.getAttributes().getNamedItem("left").getNodeValue();
									int x = Integer.parseInt(attrib);
									Log.d(tag, "parsed left value is " + x);
									if ((textbox.getX() == x) && (textbox.getY() == y)) {
										e.getFirstChild().setNodeValue(textbox.getText().toString());
										Log.d(tag, "changed text within tag to " + textbox.getText());
										mIsDirty = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}