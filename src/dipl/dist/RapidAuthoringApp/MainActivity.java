package dipl.dist.RapidAuthoringApp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
/**
 * Entrance point of the app. implements only three buttons to choose further behavior.
 * @author Johannes Holtz
 *
 */
public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/**
	 * Calls the {@link RecentFileChooser} activity. 
	 * @param value	filter type which defines which kind of files should be displayed and opened.
	 */
	private void showRFL(int value){
		Intent intent = new Intent(this, RecentFileChooser.class);
		intent.putExtra("FILTER_TYPE", value);
		startActivity(intent);
	}
	
	public void onClickRec(View v) {
		showRFL(2);
	}

	public void onClickPlay(View v) {
		showRFL(1);
	}

	public void onClickEdit(View v) throws Exception {
		showRFL(3);
	}
}
