package dipl.dist.RapidAuthoringApp;

import android.widget.RadioGroup;
import android.widget.ToggleButton;
import dipl.dist.RapidAuthoringApp.R.id;

/**
 * This class is the specialization of the {@link Toolset} class which implements the special functionality exclusive used for editing presentations
 * @author Johannes Holtz
 *
 */
public class Editor extends Toolset {
	@Override
	protected void setLayout() {
		setContentView(R.layout.activity_editor);
	}

	@Override
	protected void activateToolBtns() {
		// Tools
		ToggleButton tool = (ToggleButton) findViewById(id.BtnRecToolTextBox);
		if (mRecorderStatus == RecorderStatus.PLAYBACK) {
			tool.setEnabled(false);
		} else {
			tool.setEnabled(true);
		}
		tool = (ToggleButton) findViewById(id.BtnRecToolFreeForm);
		if (mRecorderStatus == RecorderStatus.PLAYBACK) {
			tool.setEnabled(false);
		} else {
			tool.setEnabled(true);
		}
	
		ToggleButton rec = (ToggleButton) findViewById(id.BtnRecOpStartStopp);
		rec.setEnabled(false);
	}

	@Override
	protected void setupToolBtns() {
		mSlideBtnGroup = (RadioGroup) findViewById(id.BtnGrpRecorderTools);
		mSlideBtnGroup.setOnCheckedChangeListener(this);

	}

	@Override
	protected void initInstanceSpecific() {
		// TODO Auto-generated method stub
		
	}
}
